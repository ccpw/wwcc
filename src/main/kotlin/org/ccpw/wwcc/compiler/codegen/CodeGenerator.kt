package org.ccpw.wwcc.compiler.codegen

import org.ccpw.wwcc.compiler.cast.context.GlobalContext
import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.codegen.generators.ClassGenerator
import org.objectweb.asm.ClassWriter

object CodeGenerator {
	fun generate(root: GlobalContext) =
		root.getDefinitions()
			.associate {
				generateClassFile(it)
			}

	private fun generateClassFile(clazz: ClassDefinition): Pair<String, ByteArray> {
		val writer = ClassWriter(ClassWriter.COMPUTE_MAXS + ClassWriter.COMPUTE_FRAMES)
		generateClass(clazz, writer)
		writer.visitEnd()

		return Pair(
			DescriptorUtils.classDefToFQName(clazz),
			writer.toByteArray()
		)
	}

	private fun generateClass(clazz: ClassDefinition, writer: ClassWriter) =
		ClassGenerator(clazz, writer).generate()
}