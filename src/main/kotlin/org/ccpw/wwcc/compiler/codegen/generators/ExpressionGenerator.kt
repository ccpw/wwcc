package org.ccpw.wwcc.compiler.codegen.generators

import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedVariableDeclaration
import org.ccpw.wwcc.compiler.cast.context.expression.*
import org.ccpw.wwcc.compiler.codegen.DescriptorUtils
import org.ccpw.wwcc.compiler.semantics.BuiltinTypes
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Type

class ExpressionGenerator(
	private val clazz: ClassDefinition,
	private val visitor: MethodVisitor,
) {
	fun generate(expression: Expression) {
		when (expression) {
			is Literal -> generateLiteral(expression)
			is ResolvedVariableDeclaration -> generateVariable(visitor, expression)
			is CtorInvocation -> generateCtorInvocation(expression)
			is MethodInvocation -> generateMethodInvocation(expression)
			is FieldInvocation -> generateFieldInvocation(expression)
		}
	}

	private fun generateFieldInvocation(expression: FieldInvocation) {
		val receiver = expression.targetType.reference

		generate(expression.target)

		visitor.visitFieldInsn(
			Opcodes.GETFIELD,
			DescriptorUtils.classDefToInternalName(receiver),
			expression.field.identifier,
			DescriptorUtils.varDeclToType(expression.field).descriptor
		)
	}

	private fun generateMethodInvocation(expression: MethodInvocation) {
		val receiver = expression.targetType.reference

		generate(expression.target)
		expression.args.forEach { generate(it) }

		visitor.visitMethodInsn(
			Opcodes.INVOKEVIRTUAL,
			DescriptorUtils.classDefToInternalName(receiver),
			expression.method.identifier,
			DescriptorUtils.methodDefToType(expression.method).descriptor,
			false
		)
	}

	private fun generateCtorInvocation(expression: CtorInvocation) {
		val receiver = DescriptorUtils.classDefToInternalName(expression.target)
		val ctorDef = expression.definition

		val ctorType = DescriptorUtils.methodDefToType(ctorDef)

		visitor.visitTypeInsn(
			Opcodes.NEW, receiver
		)
		visitor.visitInsn(Opcodes.DUP)

		expression.args.forEach { generate(it) }

		visitor.visitMethodInsn(
			Opcodes.INVOKESPECIAL,
			receiver, "<init>",
			ctorType.descriptor,
			false
		)
	}

	private fun generateVariable(
		visitor: MethodVisitor,
		expression: ResolvedVariableDeclaration
	) {
		when (expression.kind) {
			ResolvedVariableDeclaration.Kind.VARIABLE ->
				visitor.visitVarInsn(Opcodes.ALOAD, expression.ordinal)
			ResolvedVariableDeclaration.Kind.PARAMETER ->
				visitor.visitVarInsn(Opcodes.ALOAD, expression.ordinal)
			ResolvedVariableDeclaration.Kind.PSEUDO ->
				if (expression.identifier == "<this>")
					visitor.visitVarInsn(Opcodes.ALOAD, 0)
			ResolvedVariableDeclaration.Kind.FIELD -> {
				visitor.visitVarInsn(Opcodes.ALOAD, 0)
				visitor.visitFieldInsn(
					Opcodes.GETFIELD,
					DescriptorUtils.classDefToInternalName(clazz),
					expression.identifier,
					DescriptorUtils.varDeclToType(expression).descriptor
				)
			}
		}
	}

	private fun generateLiteral(expression: Literal) {
		when (expression) {
			is IntLiteral -> literalInstance(
				expression.value,
				BuiltinTypes.INTEGER.reference,
				Type.getMethodType(Type.VOID_TYPE, Type.INT_TYPE)
			)
			is FloatLiteral -> literalInstance(
				expression.value,
				BuiltinTypes.REAL.reference,
				Type.getMethodType(Type.VOID_TYPE, Type.DOUBLE_TYPE)
			)
			is BoolLiteral -> literalInstance(
				expression.value,
				BuiltinTypes.BOOLEAN.reference,
				Type.getMethodType(Type.VOID_TYPE, Type.BOOLEAN_TYPE)
			)
		}
	}

	private fun literalInstance(value: Any, recClass: ClassDefinition, ctorType: Type) {
		val receiver = DescriptorUtils.classDefToInternalName(recClass)

		visitor.visitTypeInsn(
			Opcodes.NEW, receiver
		)
		visitor.visitInsn(Opcodes.DUP)
		visitor.visitLdcInsn(value)
		visitor.visitMethodInsn(
			Opcodes.INVOKESPECIAL,
			receiver, "<init>",
			ctorType.descriptor,
			false
		)
	}
}