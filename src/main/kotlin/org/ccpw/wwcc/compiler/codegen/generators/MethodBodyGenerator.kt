package org.ccpw.wwcc.compiler.codegen.generators

import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedVariableDeclaration
import org.ccpw.wwcc.compiler.cast.context.expression.Expression
import org.ccpw.wwcc.compiler.cast.context.statement.*
import org.ccpw.wwcc.compiler.codegen.DescriptorUtils
import org.ccpw.wwcc.compiler.semantics.BuiltinTypes
import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Type

class MethodBodyGenerator(
	private val clazz: ClassDefinition,
	private val visitor: MethodVisitor
) {
	fun generate(block: Block) = generateCodeBlock(block)

	private fun generateCodeBlock(method: Block) {
		method.statements.forEach {
			when (it) {
				is ResolvedAssignment -> generateAssignment(it)
				is Block -> generateCodeBlock(it)
				is ResolvedIf -> generateConditional(it)
				is ResolvedLoop -> generateLoop(it)
				is ResolvedReturn -> generateReturn(it)
			}
		}
	}

	private fun generateReturn(retn: ResolvedReturn) {
		if (retn.value == null) {
			return visitor.visitInsn(Opcodes.RETURN)
		}

		generateExpression(retn.value)

		visitor.visitInsn(Opcodes.ARETURN)
	}

	private fun generateLoop(loop: ResolvedLoop) {
		val begin = Label()
		val end = Label()

		visitor.visitLabel(begin)

		generateExpression(loop.condition)
		extractBooleanScalar()

		visitor.visitJumpInsn(Opcodes.IFEQ, end)

		generateCodeBlock(loop.body)

		visitor.visitJumpInsn(Opcodes.GOTO, begin)

		visitor.visitLabel(end)

		visitor.visitInsn(Opcodes.NOP)
	}

	private fun generateAssignment(asmt: ResolvedAssignment) {
		when (asmt.target.kind) {
			ResolvedVariableDeclaration.Kind.FIELD -> generateFieldAssignment(asmt)
			ResolvedVariableDeclaration.Kind.VARIABLE -> generateVariableAssignment(asmt)
			else -> Unit
		}
	}

	private fun generateVariableAssignment(asmt: ResolvedAssignment) {
		generateExpression(asmt.value)
		visitor.visitVarInsn(Opcodes.ASTORE, asmt.target.ordinal)
	}

	private fun generateFieldAssignment(asmt: ResolvedAssignment) {
		visitor.visitVarInsn(Opcodes.ALOAD, 0)
		generateExpression(asmt.value)
		visitor.visitFieldInsn(
			Opcodes.PUTFIELD,
			DescriptorUtils.classDefToInternalName(clazz),
			asmt.target.identifier,
			DescriptorUtils.varDeclToType(asmt.target).descriptor
		)
	}

	private fun generateExpression(expr: Expression) {
		ExpressionGenerator(clazz, visitor).generate(expr)
	}

	private fun generateConditional(cond: ResolvedIf) {
		val elze = Label()
		val end = Label()

		generateExpression(cond.condition)
		extractBooleanScalar()

		visitor.visitJumpInsn(Opcodes.IFEQ, elze)

		generateCodeBlock(cond.thenBranch)

		visitor.visitJumpInsn(Opcodes.GOTO, end)
		visitor.visitLabel(elze)

		generateCodeBlock(cond.elseBranch)

		visitor.visitLabel(end)
	}

	private fun extractBooleanScalar() {
		visitor.visitFieldInsn(
			Opcodes.GETFIELD,
			DescriptorUtils.classDefToInternalName(BuiltinTypes.BOOLEAN.reference),
			"value",
			Type.BOOLEAN_TYPE.descriptor
		)
	}
}