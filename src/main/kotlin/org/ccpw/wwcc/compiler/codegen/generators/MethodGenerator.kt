package org.ccpw.wwcc.compiler.codegen.generators

import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.statement.Block
import org.ccpw.wwcc.compiler.codegen.DescriptorUtils
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes

class MethodGenerator(
	private val clazz: ClassDefinition,
	private val writer: ClassWriter
) {

	fun generate(method: ResolvedMethodDefinition) {
		when (method.identifier) {
			"<init>" -> generateCtor(method)
			else -> generateRegularMethod(method)
		}
	}

	private fun generateRegularMethod(method: ResolvedMethodDefinition) {
		val methodVisitor = writer.visitMethod(
			Opcodes.ACC_PUBLIC,
			method.identifier,
			DescriptorUtils.methodDefToType(method).descriptor,
			null, null
		)

		generateMethodBody(methodVisitor, method.body)

		methodVisitor.visitEnd()
	}

	private fun generateCtor(method: ResolvedMethodDefinition) {
		val methodVisitor = writer.visitMethod(
			Opcodes.ACC_PUBLIC,
			method.identifier,
			DescriptorUtils.methodDefToType(method).descriptor,
			null, null
		)

		generateParentCtorCall(methodVisitor)

		if (clazz.context.isDefined("<preinit>")) {
			methodVisitor.visitVarInsn(Opcodes.ALOAD, 0)
			methodVisitor.visitMethodInsn(
				Opcodes.INVOKEVIRTUAL,
				DescriptorUtils.classDefToInternalName(clazz),
				"<preinit>",
				DescriptorUtils.defaultMethodType().descriptor,
				false
			)
		}

		generateMethodBody(methodVisitor, method.body)

		methodVisitor.visitEnd()
	}

	private fun generateParentCtorCall(visitor: MethodVisitor) {
		val parentClazz =
			clazz.parent?.let { DescriptorUtils.classDefToInternalName(it) }
				?: DescriptorUtils.defaultInternalName()

		val ctor = clazz.parent?.context?.getDefinition("<init>")

		val ctorDesc = if (ctor is ResolvedMethodDefinition) {
			DescriptorUtils.methodDefToType(ctor)
		} else {
			DescriptorUtils.defaultMethodType()
		}

		visitor.visitVarInsn(Opcodes.ALOAD, 0)

		visitor.visitMethodInsn(
			Opcodes.INVOKESPECIAL,
			parentClazz,
			"<init>",
			ctorDesc.descriptor,
			false
		)
	}

	private fun generateMethodBody(visitor: MethodVisitor, block: Block) =
		MethodBodyGenerator(clazz, visitor).generate(block)
}