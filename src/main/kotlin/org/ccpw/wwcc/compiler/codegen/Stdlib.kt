package org.ccpw.wwcc.compiler.codegen

import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import java.io.File
import java.nio.file.Files
import java.nio.file.Path

object Stdlib {
	private const val STDLIB_DIR = "stdlib/"

	private fun stdlib(): Path = Path.of(STDLIB_DIR)

	fun compileBuiltins(): Map<String, ByteArray> {
		return Files.newDirectoryStream(stdlib())
			.asSequence()
			.filter { Files.isRegularFile(it) }
			.map { it.toAbsolutePath().toString() }
			.filter { it.endsWith(".class") }
			.map { File(it) }
			.associate {
				propagateClass(it)
			}
	}

	private fun propagateClass(file: File): Pair<String, ByteArray> {
		val reader = ClassReader(file.inputStream())
		val writer = ClassWriter(reader, ClassWriter.COMPUTE_FRAMES + ClassWriter.COMPUTE_MAXS)
		reader.accept(writer, 0)


		return (reader.className to writer.toByteArray())
	}
}