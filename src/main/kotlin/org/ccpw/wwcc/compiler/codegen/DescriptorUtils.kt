package org.ccpw.wwcc.compiler.codegen

import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedVariableDeclaration
import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedType
import org.objectweb.asm.Type

object DescriptorUtils {
	private const val CLASS_MANGLED_PREFIX = "wwc.lang.WWC__"

	fun classDefToFQName(clazz: ClassDefinition) = "${CLASS_MANGLED_PREFIX}${clazz.identifier}"

	fun defaultInternalName(): String = Type.getInternalName(java.lang.Object::class.java)

	fun classDefToInternalName(clazz: ClassDefinition) =
		classDefToFQName(clazz).replace('.', '/')

	fun varDeclToType(varDecl: ResolvedVariableDeclaration) =
		Type.getObjectType(
			classDefToInternalName(varDecl.type.reference)
		)

	fun resolvedTypeToType(type: ResolvedType): Type =
		Type.getObjectType(classDefToInternalName(type.reference))

	fun methodDefToType(method: ResolvedMethodDefinition): Type {
		val args = method.params
			.map { resolvedTypeToType(it.type) }
			.toTypedArray()

		return Type.getMethodType(
			returnTypeToType(method.returnType),
			*args
		)
	}

	private fun returnTypeToType(type: ResolvedType?): Type =
		type?.let { resolvedTypeToType(it) } ?: Type.VOID_TYPE

	fun defaultMethodType(): Type = Type.getMethodType(Type.VOID_TYPE)
}