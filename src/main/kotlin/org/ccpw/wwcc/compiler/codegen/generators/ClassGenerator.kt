package org.ccpw.wwcc.compiler.codegen.generators

import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedOvlMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedVariableDeclaration
import org.ccpw.wwcc.compiler.codegen.DescriptorUtils
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Opcodes

class ClassGenerator(
	private val clazz: ClassDefinition,
	private val writer: ClassWriter
) {
	fun generate() {
		writer.visit(
			Opcodes.V11, Opcodes.ACC_PUBLIC,
			DescriptorUtils.classDefToInternalName(clazz),
			null,
			clazz.parent?.let { DescriptorUtils.classDefToInternalName(it) },
			null
		)

		clazz
			.context.getDefinitions()
			.forEach {
				when (it) {
					is ResolvedVariableDeclaration -> {
						if (it.identifier == "<this>") return@forEach

						generateField(writer, it)
					}
					is ResolvedOvlMethodDefinition -> {
						it.map { MethodGenerator(clazz, writer).generate(it) }
					}
				}
			}
	}

	private fun generateField(
		writer: ClassWriter,
		varDecl: ResolvedVariableDeclaration
	) {
		val fieldVisitor = writer.visitField(
			Opcodes.ACC_PUBLIC,
			varDecl.identifier,
			DescriptorUtils.resolvedTypeToType(varDecl.type).descriptor,
			null, null
		)
	}
}