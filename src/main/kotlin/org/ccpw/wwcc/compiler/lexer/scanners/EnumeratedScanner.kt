package org.ccpw.wwcc.compiler.lexer.scanners

import org.ccpw.wwcc.compiler.lexer.lexeme.Lexeme
import org.ccpw.wwcc.compiler.lexer.lexeme.LexemeType
import org.ccpw.wwcc.util.StringUtils

abstract class EnumeratedScanner(
	enum: List<String>,
	private val type: LexemeType
) : Scanner() {
	private val startSymbols = enum.map { keyword -> keyword[0] }.joinToString("")
	private val allowedSymbols = StringUtils.uniqueStringChars(enum.joinToString(""))

	private var suffixValid: Boolean = false
	private var triedSuffix: Boolean = false
	private var current = StringBuilder("")
	private var matches = enum.toList()

	override fun canStartWith(char: Char): Boolean {
		return (char in startSymbols)
	}

	protected abstract fun validSuffix(char: Char): Boolean

	override fun nextChar(char: Char): Boolean {
		if (char !in allowedSymbols) {
			triedSuffix = true

			if (validSuffix(char)) suffixValid = true

			return false
		}

		current.append(char)

		matches = matches.filter { kw -> kw.startsWith(current.toString()) }

		if (matches.isEmpty()) return false

		return true
	}

	override fun finalize(): Lexeme {
		val string = current.toString()

		if (matches.size != 1 || matches[0] != string) {
			return Lexeme(LexemeType.I_UNDEFINED, string)
		}

		if (triedSuffix) {
			if (!suffixValid) {
				return Lexeme(LexemeType.I_UNDEFINED, string)
			}
		}

		return Lexeme(type, string)
	}
}