package org.ccpw.wwcc.compiler.lexer.scanners

import org.ccpw.wwcc.compiler.lexer.lexeme.Lexeme
import org.ccpw.wwcc.compiler.lexer.lexeme.LexemeType

class SequenceScanner(val string: String) {
	private val current = StringBuilder("")

	fun canStartWith(char: Char): Boolean {
		return string[0] == char
	}

	fun nextChar(char: Char): Boolean {
		val shift = current.length

		if (shift >= string.length) return false
		if (string[shift] != char) return false

		current.append(char)

		return true
	}

	fun finalize(): Lexeme {
		val scanned = current.toString()

		current.clear()

		if (scanned == string) {
			return Lexeme(LexemeType.I_SEQUENCE, string)
		}

		return Lexeme(LexemeType.I_UNDEFINED, scanned)
	}
}