package org.ccpw.wwcc.compiler.lexer.scanners

import org.ccpw.wwcc.compiler.lexer.lexeme.LexemeType

class BooleanScanner : EnumeratedScanner(
	listOf(
		"true",
		"false"
	),
	LexemeType.L_BOOL
) {
	override fun validSuffix(char: Char): Boolean {
		return (WhitespaceScanner().canStartWith(char) || (char == '.') || PunctuationScanner().canStartWith(char))
	}
}