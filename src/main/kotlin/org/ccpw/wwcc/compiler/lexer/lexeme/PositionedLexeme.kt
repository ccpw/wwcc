package org.ccpw.wwcc.compiler.lexer.lexeme

import kotlinx.serialization.Serializable
import org.ccpw.wwcc.util.RayInterval

@Serializable
data class PositionedLexeme(
	val type: LexemeType,
	val value: String?,
	val position: RayInterval
) {
	companion object {
		fun fromLexeme(
			lexeme: Lexeme,
			start: Int,
			end: Int
		): PositionedLexeme {
			return PositionedLexeme(
				lexeme.type,
				lexeme.value,
				RayInterval(start, end)
			)
		}
	}
}