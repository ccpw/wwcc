package org.ccpw.wwcc.compiler.lexer.scanners

import org.ccpw.wwcc.compiler.lexer.lexeme.Lexeme
import org.ccpw.wwcc.compiler.lexer.lexeme.LexemeType

class AssignmentScanner : Scanner() {
	companion object {
		private val seqScanner = SequenceScanner(":=")
	}

	override fun canStartWith(char: Char): Boolean {
		return seqScanner.canStartWith(char)
	}

	override fun nextChar(char: Char): Boolean {
		return seqScanner.nextChar(char)
	}

	override fun finalize(): Lexeme {
		val lexeme = seqScanner.finalize()

		if (lexeme.type != LexemeType.I_UNDEFINED) {
			return Lexeme(LexemeType.P_ASSIGNMENT, ":=")
		}

		return lexeme
	}
}