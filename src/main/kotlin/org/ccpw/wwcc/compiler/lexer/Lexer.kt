package org.ccpw.wwcc.compiler.lexer

import org.ccpw.wwcc.compiler.lexer.lexeme.PositionedLexeme

interface Lexer {
	fun process(): List<PositionedLexeme>
}