package org.ccpw.wwcc.compiler.lexer.lexeme

data class Lexeme(
	val type: LexemeType,
	val value: String?
)