package org.ccpw.wwcc.compiler.lexer.scanners

import org.ccpw.wwcc.compiler.lexer.lexeme.Lexeme

abstract class Scanner {
	abstract fun canStartWith(char: Char): Boolean
	abstract fun nextChar(char: Char): Boolean
	abstract fun finalize(): Lexeme
}