package org.ccpw.wwcc.compiler.lexer

import org.ccpw.wwcc.compiler.lexer.lexeme.PositionedLexeme

class UndefinedLexemeException(val lexeme: PositionedLexeme) : RuntimeException() {
	override val message: String?
		get() = "error: undefined token '${lexeme.value}' at shift ${lexeme.position.start}"
}