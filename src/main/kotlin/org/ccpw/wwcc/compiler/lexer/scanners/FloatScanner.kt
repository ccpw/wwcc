package org.ccpw.wwcc.compiler.lexer.scanners

import org.ccpw.wwcc.compiler.lexer.lexeme.Lexeme
import org.ccpw.wwcc.compiler.lexer.lexeme.LexemeType

class FloatScanner : Scanner() {
	private val current = StringBuilder("")
	private var subdecimal = false

	override fun canStartWith(char: Char): Boolean {
		if (char in '0'..'9') return true
		if (char in "-.") return true

		return false
	}

	override fun nextChar(char: Char): Boolean {
		if (!isCorrectChar(char)) return false

		current.append(char)

		if (char == '.') subdecimal = true

		return true
	}

	override fun finalize(): Lexeme {
		if (!subdecimal) return Lexeme(LexemeType.I_UNDEFINED, current.toString())

		return Lexeme(LexemeType.L_FLOAT, current.toString())
	}

	private fun isCorrectChar(char: Char): Boolean {
		if (current.isEmpty() && canStartWith(char)) return true
		if (char in '0'..'9') return true
		if (!subdecimal && char == '.') return true

		return false
	}
}