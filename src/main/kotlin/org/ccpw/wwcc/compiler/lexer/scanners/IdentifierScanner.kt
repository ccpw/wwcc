package org.ccpw.wwcc.compiler.lexer.scanners

import org.ccpw.wwcc.compiler.lexer.lexeme.Lexeme
import org.ccpw.wwcc.compiler.lexer.lexeme.LexemeType

class IdentifierScanner : Scanner() {
	private val string = StringBuilder("")

	override fun canStartWith(char: Char): Boolean {
		if (char in 'a'..'z') return true
		if (char in 'A'..'Z') return true
		if (char in "_$") return true

		return false
	}

	override fun nextChar(char: Char): Boolean {
		if (!isCorrectChar(char)) return false

		string.append(char)

		return true
	}

	override fun finalize(): Lexeme {
		return Lexeme(
			LexemeType.IDENTIFIER,
			string.toString()
		)
	}

	private fun isCorrectChar(char: Char): Boolean {
		if (canStartWith(char)) return true
		if (char in '0'..'9') return true

		return false
	}
}