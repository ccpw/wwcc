package org.ccpw.wwcc.compiler.lexer

import org.ccpw.wwcc.compiler.lexer.lexeme.Lexeme
import org.ccpw.wwcc.compiler.lexer.lexeme.LexemeType
import org.ccpw.wwcc.compiler.lexer.lexeme.PositionedLexeme
import org.ccpw.wwcc.compiler.lexer.scanners.*
import org.ccpw.wwcc.util.RayInterval
import org.ccpw.wwcc.util.StringScanner
import java.io.InputStream

class RawLexer(input: InputStream) : Lexer {
	private val stringScanner = StringScanner(input)

	override fun process(): List<PositionedLexeme> {
		val lexemes = ArrayList<PositionedLexeme>()

		while (true) {
			val lexeme = nextLexeme()

			if (lexeme.type == LexemeType.I_EOF) break
			if (lexeme.type == LexemeType.I_IGNORED) continue
			if (lexeme.type == LexemeType.I_UNDEFINED) throw UndefinedLexemeException(lexeme)

			lexemes.add(lexeme)
		}

		return lexemes
	}

	private fun applyScanner(scanner: Scanner, lastChar: Char): Lexeme {
		var char: Char = lastChar
		var scanned = 0

		if (scanner.canStartWith(char)) {
			while (true) {
				if (!scanner.nextChar(char)) {
					scanned--
					stringScanner.unwind(1)
					break
				}
				scanned++
				char = stringScanner.next() ?: 0.toChar()

				if (char == 0.toChar()) {
					scanned-- // we didn't actually move if we reached EOF
					break
				}
			}

			val lexeme = scanner.finalize()

			if (lexeme.type != LexemeType.I_UNDEFINED) return lexeme
		}

		stringScanner.unwind(scanned)

		return Lexeme(LexemeType.I_UNDEFINED, char.toString())
	}

	private fun nextLexeme(): PositionedLexeme {
		while (true) {
			val start = stringScanner.position()
			val char = stringScanner.next() ?: return PositionedLexeme(
				LexemeType.I_EOF, null,
				RayInterval(start, stringScanner.position())
			)

			val scanners = listOf(
				WhitespaceScanner(),
				KeywordScanner(),
				AssignmentScanner(),
				PunctuationScanner(),
				BooleanScanner(),
				FloatScanner(),
				IntegerScanner(),
				IdentifierScanner()
			)

			var lexeme: Lexeme

			for (scanner in scanners) {
				lexeme = applyScanner(scanner, char)
				if (lexeme.type != LexemeType.I_UNDEFINED) {
					return PositionedLexeme.fromLexeme(
						lexeme,
						start, stringScanner.position()
					)
				}
			}

			return PositionedLexeme(
				LexemeType.I_UNDEFINED, "$char",
				RayInterval(start, stringScanner.position())
			)
		}
	}
}