package org.ccpw.wwcc.compiler.lexer.scanners

import org.ccpw.wwcc.compiler.lexer.lexeme.Lexeme
import org.ccpw.wwcc.compiler.lexer.lexeme.LexemeType

class PunctuationScanner : Scanner() {
	companion object {
		private val signMap = mapOf(
			'[' to LexemeType.P_LBRACKET,
			']' to LexemeType.P_RBRACKET,
			':' to LexemeType.P_COLON,
			'.' to LexemeType.P_FSTOP,
			',' to LexemeType.P_COMMA,
			'(' to LexemeType.P_LPAREN,
			')' to LexemeType.P_RPAREN
		)
	}

	private var current: Char? = null

	override fun canStartWith(char: Char): Boolean {
		return (char in signMap.keys)
	}

	override fun nextChar(char: Char): Boolean {
		if (current != null) return false
		current = char
		return true
	}

	override fun finalize(): Lexeme {
		val type = signMap[current] ?: LexemeType.I_UNDEFINED
		return Lexeme(type, current.toString())
	}
}