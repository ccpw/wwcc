package org.ccpw.wwcc.compiler.lexer.scanners

import org.ccpw.wwcc.compiler.lexer.lexeme.Lexeme
import org.ccpw.wwcc.compiler.lexer.lexeme.LexemeType

class KeywordScanner : EnumeratedScanner(
	listOf(
		"class",
		"extends",
		"is",
		"end",
		"var",
		"method",
		"this",
		"while",
		"loop",
		"if",
		"then",
		"else",
		"return"
	),
	LexemeType.I_KEYWORD
) {
	override fun validSuffix(char: Char): Boolean {
		return WhitespaceScanner().canStartWith(char) || PunctuationScanner().canStartWith(char)
	}

	override fun finalize(): Lexeme {
		val lx = super.finalize()

		if (lx.type == LexemeType.I_UNDEFINED) return lx

		val type = when (lx.value) {
			"class" -> LexemeType.K_CLASS
			"extends" -> LexemeType.K_EXTENDS
			"is" -> LexemeType.K_IS
			"end" -> LexemeType.K_END
			"var" -> LexemeType.K_VAR
			"method" -> LexemeType.K_METHOD
			"this" -> LexemeType.K_THIS
			"while" -> LexemeType.K_WHILE
			"loop" -> LexemeType.K_LOOP
			"if" -> LexemeType.K_IF
			"then" -> LexemeType.K_THEN
			"else" -> LexemeType.K_ELSE
			"return" -> LexemeType.K_RETURN
			else -> LexemeType.I_UNDEFINED
		}

		return Lexeme(
			type, value = lx.value
		)
	}
}