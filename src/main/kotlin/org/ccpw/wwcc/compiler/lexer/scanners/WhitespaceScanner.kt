package org.ccpw.wwcc.compiler.lexer.scanners

import org.ccpw.wwcc.compiler.lexer.lexeme.Lexeme
import org.ccpw.wwcc.compiler.lexer.lexeme.LexemeType

class WhitespaceScanner : Scanner() {
	companion object {
		private const val symbols = " \t\r\n"
	}

	override fun canStartWith(char: Char): Boolean {
		return (char in symbols)
	}

	override fun nextChar(char: Char): Boolean {
		return (char in symbols)
	}

	override fun finalize(): Lexeme {
		return Lexeme(LexemeType.I_IGNORED, null)
	}
}