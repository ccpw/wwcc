package org.ccpw.wwcc.compiler.lexer.scanners

import org.ccpw.wwcc.compiler.lexer.lexeme.Lexeme
import org.ccpw.wwcc.compiler.lexer.lexeme.LexemeType

class IntegerScanner : Scanner() {
	private val current = StringBuilder("")

	override fun canStartWith(char: Char): Boolean {
		if (char in '0'..'9') return true
		if (char == '-') return true

		return false
	}

	override fun nextChar(char: Char): Boolean {
		if (!isCorrectChar(char)) return false

		current.append(char)

		return true
	}

	override fun finalize(): Lexeme {
		return Lexeme(LexemeType.L_INT, current.toString())
	}

	private fun isCorrectChar(char: Char): Boolean {
		if (current.isEmpty() && canStartWith(char)) return true
		if (char in '0'..'9') return true

		return false
	}
}