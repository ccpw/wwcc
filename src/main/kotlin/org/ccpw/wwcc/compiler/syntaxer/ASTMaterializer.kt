package org.ccpw.wwcc.compiler.syntaxer

import org.ccpw.wwcc.compiler.ast.*
import org.ccpw.wwcc.compiler.ast.ghost.GhostASTNode
import org.ccpw.wwcc.compiler.ast.ghost.GhostASTNodeType
import org.ccpw.wwcc.compiler.lexer.lexeme.LexemeType
import org.ccpw.wwcc.compiler.lexer.lexeme.PositionedLexeme

object ASTMaterializer {
	private data class InvocationMember(
		val identifier: IdentifierNode,
		val args: List<ExpressionNode>
	)

	fun materialize(node: GhostASTNode): RootNode? {
		return when (node.nodeType) {
			GhostASTNodeType.ROOT -> materializeRoot(node)
			else -> null
		}
	}

	private fun materializeRoot(node: GhostASTNode): RootNode {
		val classes = node.children.map { child -> materializeClass(child) }

		return RootNode(classes)
	}

	private fun materializeClass(node: GhostASTNode): ClassNode {
		require(node.children.size == 3) { -> "error: internal error" }

		val name = materializeClassNameStrict(node.children[0])
		val parent = materializeClassName(node.children[1])
		val body = materializeClassBody(node.children[2])

		return ClassNode(name, parent, body)
	}

	private fun materializeClassNameStrict(node: GhostASTNode): ClassNameNode {
		val className = materializeClassName(node)

		requireNotNull(className) { -> "error: invalid classname" }

		return className
	}

	private fun materializeClassName(node: GhostASTNode): ClassNameNode? {
		if (node.children.isEmpty()) return null

		val identifier = materializeIdentifier(node.children[0])
		var genericArg: ClassNameNode? = null

		if (node.children.size >= 2)
			genericArg = materializeClassName(node.children[1])

		return ClassNameNode(identifier, genericArg)
	}

	private fun materializeIdentifier(node: GhostASTNode): IdentifierNode {
		requireNotNull(node.value) { -> "error: internal error" }
		requireNotNull(node.value.value) { -> "error: invalid identifier at shift ${node.value.position.start}" }

		return IdentifierNode(node.value.value)
	}

	private fun materializeClassBody(node: GhostASTNode): List<DefinitionNode> {
		return node.children.map { child -> materializeDefinition(child) }
	}

	private fun materializeDefinition(node: GhostASTNode): DefinitionNode {
		return when (node.nodeType) {
			GhostASTNodeType.VAR_DECL -> materializeVariableDeclaration(node)
			GhostASTNodeType.METHOD -> materializeMethod(node)
			GhostASTNodeType.CTOR -> materializeCtor(node)
			else -> error("error: invalid statement in class body")
		}
	}

	private fun materializeCtor(node: GhostASTNode): CtorDefinitionNode {
		require(node.children.size == 2) { "error: invalid ctor declaration" }

		val params = materializeParameters(node.children[0])
		val body = materializeBlock(node.children[1])

		return CtorDefinitionNode(params, body)
	}

	private fun materializeMethod(node: GhostASTNode): MethodDefinitionNode {
		require(node.children.size == 4) { "error: invalid method declaration" }

		val identifier = materializeIdentifier(node.children[0])
		val params = materializeParameters(node.children[1])
		val type = materializeClassName(node.children[2])
		val body = materializeBlock(node.children[3])

		return MethodDefinitionNode(identifier, params, type, body)
	}

	private fun materializeBlock(node: GhostASTNode): BlockNode {
		val statements = node.children.map { child -> materializeStatement(child) }

		return BlockNode(statements)
	}

	private fun materializeStatement(node: GhostASTNode): StatementNode {
		return when (node.nodeType) {
			GhostASTNodeType.CONDITIONAL -> materializeIfClause(node)
			GhostASTNodeType.LOOP -> materializeWhileClause(node)
			GhostASTNodeType.VAR_DECL -> materializeVariableDeclaration(node)
			GhostASTNodeType.RETURN -> materializeReturn(node)
			GhostASTNodeType.ASSIGNMENT -> materializeAssignment(node)
			else -> error("error: statement expected")
		}
	}

	private fun materializeAssignment(node: GhostASTNode): AssignmentNode {
		require(node.children.size == 2) { "error: invalid assignment" }

		val target = materializeIdentifier(node.children[0])
		val value = materializeExpression(node.children[1])

		return AssignmentNode(target, value)
	}

	private fun materializeReturn(node: GhostASTNode): ReturnNode {
		var value: ExpressionNode? = null

		if (node.children.isNotEmpty()) value = materializeExpression(node.children[0])

		return ReturnNode(value)
	}

	private fun materializeIfClause(node: GhostASTNode): IfClauseNode {
		require(node.children.size == 3) { "error: invalid if clause" }

		val condition = materializeExpression(node.children[0])
		val thenBranch = materializeBlock(node.children[1])
		val elseBranch = materializeBlock(node.children[2])

		return IfClauseNode(condition, thenBranch, elseBranch)
	}

	private fun materializeWhileClause(node: GhostASTNode): WhileClauseNode {
		require(node.children.size == 2) { "error: invalid if clause" }

		val condition = materializeExpression(node.children[0])
		val body = materializeBlock(node.children[1])

		return WhileClauseNode(condition, body)
	}

	private fun materializeParameters(node: GhostASTNode): List<MethodParameterNode> {
		return node.children.map { child -> materializeParameter(child) }
	}

	private fun materializeParameter(node: GhostASTNode): MethodParameterNode {
		require(node.children.size == 2) { "error: invalid parameter definition" }

		val identifier = materializeIdentifier(node.children[0])
		val type = materializeClassNameStrict(node.children[1])

		return MethodParameterNode(identifier, type)
	}

	private fun materializeVariableDeclaration(node: GhostASTNode): VariableDeclarationNode {
		require(node.children.size == 2) { "error: invalid variable declaration" }

		val identifier = materializeIdentifier(node.children[0])
		val initializer = materializeExpression(node.children[1])

		return VariableDeclarationNode(
			identifier, initializer
		)
	}

	private fun materializeExpression(node: GhostASTNode): ExpressionNode {
		return when (node.nodeType) {
			GhostASTNodeType.KEYWORD -> materializeThis(node)
			GhostASTNodeType.IDENTIFIER -> materializeIdentifier(node)
			GhostASTNodeType.LITERAL -> materializeLiteral(node)
			GhostASTNodeType.CTOR_CALL -> materializeCtorCall(node)
			GhostASTNodeType.INVOCATION -> materializeInvocation(node)

			else -> error("error: invalid expression")
		}
	}

	private fun materializeInvocation(node: GhostASTNode): ExpressionNode {
		require(node.children.size == 2) { "error : invalid member call" }

		val target = materializeExpression(node.children[0])
		val member = materializeInvocationMember(node.children[1])

		return MemberInvocationNode(
			target,
			member = member.identifier,
			arguments = member.args
		)
	}

	private fun materializeInvocationMember(node: GhostASTNode): InvocationMember {
		require(node.children.size == 2) { "error : invalid member call" }

		val member = materializeIdentifier(node.children[0])
		val args = materializeInvocationArguments(node.children[1])

		return InvocationMember(member, args)
	}

	private fun materializeCtorCall(node: GhostASTNode): CtorInvocationNode {
		require(node.children.size == 2) { "error: invalid ctor call" }

		val target = materializeClassNameStrict(node.children[0])
		val args = materializeInvocationArguments(node.children[1])

		return CtorInvocationNode(target, args)
	}

	private fun materializeInvocationArguments(node: GhostASTNode): List<ExpressionNode> {
		return node.children.map { child -> materializeExpression(child) }
	}

	private fun materializeLiteral(node: GhostASTNode): LiteralNode {
		requireNotNull(node.value) { "error: internal error" }

		return when (node.value.type) {
			LexemeType.L_BOOL -> materializeBool(node.value)
			LexemeType.L_INT -> materializeInt(node.value)
			LexemeType.L_FLOAT -> materializeFloat(node.value)

			else -> error("error: internal error")
		}
	}

	private fun materializeInt(node: PositionedLexeme): IntLiteralNode {
		requireNotNull(node.value) { "error: internal error" }

		return IntLiteralNode(node.value.toInt())
	}

	private fun materializeFloat(node: PositionedLexeme): FloatLiteralNode {
		requireNotNull(node.value) { "error: internal error" }

		return FloatLiteralNode(node.value.toDouble())
	}

	private fun materializeBool(node: PositionedLexeme): BoolLiteralNode {
		requireNotNull(node.value) { "error: internal error" }

		return when (node.value) {
			"true" -> BoolLiteralNode(true)
			"false" -> BoolLiteralNode(false)

			else -> error("error: internal error")
		}
	}

	private fun materializeThis(node: GhostASTNode): ThisNode {
		requireNotNull(node.value) { "error: internal error" }

		require(node.value.type == LexemeType.K_THIS) { "error: internal error" }

		return ThisNode()
	}
}