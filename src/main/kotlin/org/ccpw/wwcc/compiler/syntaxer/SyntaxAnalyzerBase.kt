package org.ccpw.wwcc.compiler.syntaxer

import org.ccpw.wwcc.compiler.ast.ghost.GhostASTNode
import org.ccpw.wwcc.compiler.ast.ghost.GhostASTNodeType
import org.ccpw.wwcc.compiler.lexer.lexeme.PositionedLexeme
import org.ccpw.wwcc.util.ListScanner

abstract class SyntaxAnalyzerBase {
	protected var lscanner: ListScanner<PositionedLexeme>? = null

	fun useLexemes(lexemes: List<PositionedLexeme>) {
		lscanner = ListScanner(lexemes)
	}

	protected fun <E> listOf(vararg elements: E): List<E> {
		val list = mutableListOf<E>()

		for (el in elements) {
			list.add(el)
		}

		return list
	}

	protected fun newNode(
		nodeType: GhostASTNodeType,
		children: List<GhostASTNode> = listOf()
	): GhostASTNode {
		return GhostASTNode(nodeType, null, children)
	}

	protected fun newNode(
		nodeType: GhostASTNodeType,
		value: PositionedLexeme
	): GhostASTNode {
		return GhostASTNode(nodeType, value, listOf())
	}

	protected fun emptyBlock(): GhostASTNode {
		return newNode(GhostASTNodeType.BLOCK, listOf())
	}
}