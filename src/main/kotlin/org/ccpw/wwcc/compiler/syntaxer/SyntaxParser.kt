package org.ccpw.wwcc.compiler.syntaxer

import org.ccpw.wwcc.cli.RuntimeOptions
import org.ccpw.wwcc.compiler.ast.RootNode
import org.ccpw.wwcc.compiler.lexer.lexeme.PositionedLexeme

object SyntaxParser {
	fun process(lexemes: List<PositionedLexeme>, options: RuntimeOptions): RootNode? {
		val syntaxer = SyntaxAnalyzerGenerated(options.verbose)

		syntaxer.useLexemes(lexemes)

		val shadowTree = syntaxer.process()

		return ASTMaterializer.materialize(shadowTree)
	}
}