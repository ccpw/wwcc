package org.ccpw.wwcc.compiler.semantics

import org.ccpw.wwcc.compiler.ast.RootNode
import org.ccpw.wwcc.compiler.cast.context.GlobalContext

object CASTPipeline {
	fun process(ast: RootNode, stdlib: RootNode): GlobalContext {
		val gAst = RootNode(
			classes = ast.classes + stdlib.classes
		)

		var cast = CASTBuilder.process(gAst)

		cast = CASTTypeResolver.process(cast)
		cast = CASTInitializerTypeResolver.process(cast)
		cast = CASTExpressionResolver.process(cast)
		cast = CASTAssignmentResolver.process(cast)
		cast = CASTControlsResolver.process(cast)

		return cast
	}
}