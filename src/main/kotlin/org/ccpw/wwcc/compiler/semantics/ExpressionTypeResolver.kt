package org.ccpw.wwcc.compiler.semantics

import org.ccpw.wwcc.compiler.ast.*
import org.ccpw.wwcc.compiler.cast.context.Context
import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.Definition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedOvlMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedVariableDeclaration
import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedType

class ExpressionTypeResolver(
	val context: Context
) {
	fun resolve(expression: ExpressionNode): ResolvedType {
		return when (expression) {
			is LiteralNode -> resolveLiteral(expression)
			is IdentifierNode -> resolveVariable(expression)
			is ThisNode -> resolveThis()
			is CtorInvocationNode -> resolveCtorInvocation(expression)
			is MemberInvocationNode -> resolveMemberInvocation(expression)
			else -> error("Invalid expression")
		}
	}

	private fun resolveCtorInvocation(expression: CtorInvocationNode): ResolvedType {
		val className = expression.targetClass.identifier.identifier
		val resolved = context.getDefinition(className)

		require(resolved is ClassDefinition) { "'$className' class is not defined" }

		return ResolvedType(resolved)
	}

	private fun resolveMemberInvocation(expression: MemberInvocationNode): ResolvedType {
		val targetType = resolve(expression.target)
		val clasCtx = targetType.reference.context

		val resolved = clasCtx.getDefinition(expression.member.identifier)

		if (expression.arguments.isNotEmpty()) {
			return resolveMethodInvocation(resolved, expression)
		} else {
			return if (resolved is ResolvedOvlMethodDefinition)
				resolveMethodInvocation(resolved, expression)
			else {
				require(resolved is ResolvedVariableDeclaration) { "${resolved.identifier} is not a variable" }

				resolved.type
			}
		}
	}

	private fun resolveMethodInvocation(
		resolved: Definition,
		expression: MemberInvocationNode
	): ResolvedType {
		require(resolved is ResolvedOvlMethodDefinition) { "'${resolved.identifier}' is not a function" }

		val meth = resolved.getOverload(
			expression.arguments.map { resolve(it).reference }
		)
		return checkNotNull(meth.returnType) { "Method does not return anything" }
	}

	private fun resolveThis(): ResolvedType {
		val definition = context.getDefinition("<this>")

		require(definition is ResolvedVariableDeclaration) { "'this' is not a variable" }

		return definition.type
	}

	private fun resolveVariable(identifier: IdentifierNode): ResolvedType {
		val definition = context.getDefinition(identifier.identifier)

		require(definition is ResolvedVariableDeclaration) { "${identifier.identifier} is not a variable" }

		return definition.type
	}

	// private

	private fun resolveLiteral(literal: LiteralNode) =
		when (literal) {
			is IntLiteralNode -> BuiltinTypes.INTEGER
			is FloatLiteralNode -> BuiltinTypes.REAL
			is BoolLiteralNode -> BuiltinTypes.BOOLEAN
			else -> error("Invalid literal")
		}
}