package org.ccpw.wwcc.compiler.semantics

import org.ccpw.wwcc.compiler.cast.context.ClassContext
import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedType

object BuiltinTypes {
	val INTEGER = ResolvedType(
		reference = ClassDefinition(
			"Integer",
			ClassContext()
		)
	)

	val REAL = ResolvedType(
		reference = ClassDefinition(
			"Real",
			ClassContext()
		)
	)

	val BOOLEAN = ResolvedType(
		reference = ClassDefinition(
			"Boolean",
			ClassContext()
		)
	)

	fun getTypes() = listOf(INTEGER, REAL, BOOLEAN)
}