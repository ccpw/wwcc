package org.ccpw.wwcc.compiler.semantics

import org.ccpw.wwcc.compiler.cast.context.BlockContext
import org.ccpw.wwcc.compiler.cast.context.Context
import org.ccpw.wwcc.compiler.cast.context.GlobalContext
import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedOvlMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.statement.*

object CASTExpressionResolver {
	fun process(cast: GlobalContext) =
		scanGlobal(cast)

	private fun scanGlobal(cast: GlobalContext): GlobalContext {
		cast.getDefinitions()
			.forEach { scanClass(it, cast) }

		return cast
	}

	private fun scanClass(clazz: ClassDefinition, context: Context): ClassDefinition {
		clazz.context.getDefinitions()
			.map {
				when (it) {
					is ResolvedOvlMethodDefinition -> scanOverload(it, clazz.context)
					else -> it
				}
			}
			.forEach { clazz.context.overwriteMember(it) }

		return clazz
	}

	private fun scanOverload(
		ovl: ResolvedOvlMethodDefinition,
		context: Context
	): ResolvedOvlMethodDefinition {
		ovl
			.map { scanMethod(it, context) }
			.forEach { ovl.overwriteOverload(it) }

		return ovl
	}

	private fun scanMethod(definition: ResolvedMethodDefinition, context: Context): ResolvedMethodDefinition {
		return ResolvedMethodDefinition(
			identifier = definition.identifier,
			params = definition.params,
			returnType = definition.returnType,
			body = scanBlock(definition.body)
		)
	}

	private fun scanBlock(block: Block): Block {
		val stmts = block.statements.map {
			when (it) {
				is Assignment -> scanAssignment(it, block.context)
				is Loop -> scanLoop(it, block.context)
				is Conditional -> scanCond(it, block.context)
				is ReturnStatement -> scanReturn(it, block.context)
				is Block -> scanBlock(it)
				else -> it
			}
		}

		return Block(
			statements = stmts,
			context = block.context
		)
	}

	private fun scanReturn(ret: ReturnStatement, context: BlockContext): Statement {
		return ReturnStatement(
			valueNode = ret.valueNode?.let { ExpressionResolver(context).resolve(it) }
		)
	}

	private fun scanCond(cond: Conditional, context: BlockContext): Statement {
		return Conditional(
			conditionNode = ExpressionResolver(context).resolve(cond.conditionNode),
			truthyBranch = scanBlock(cond.truthyBranch),
			falseyBranch = scanBlock(cond.falseyBranch),
		)
	}

	private fun scanLoop(loop: Loop, context: BlockContext): Loop {
		return Loop(
			conditionNode = ExpressionResolver(context).resolve(loop.conditionNode),
			body = scanBlock(loop.body)
		)
	}

	private fun scanAssignment(asmt: Assignment, context: BlockContext) =
		Assignment(
			target = asmt.target,
			valueNode = ExpressionResolver(context).resolve(asmt.valueNode)
		)
}