package org.ccpw.wwcc.compiler.semantics

import org.ccpw.wwcc.compiler.ast.ClassNameNode
import org.ccpw.wwcc.compiler.cast.context.ClassContext
import org.ccpw.wwcc.compiler.cast.context.Context
import org.ccpw.wwcc.compiler.cast.context.GlobalContext
import org.ccpw.wwcc.compiler.cast.context.definition.*
import org.ccpw.wwcc.compiler.cast.context.statement.Block
import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedType

object CASTTypeResolver {
	fun process(cast: GlobalContext) =
		scanGlobal(cast)

	private fun scanGlobal(cast: GlobalContext): GlobalContext {
		// BuiltinTypes.getTypes().forEach {
		// 	cast.defineClass(it.reference)
		// }

		cast.getDefinitions()
			.forEach { scanClass(it, cast) }

		return cast
	}

	private fun scanClass(clazz: ClassDefinition, context: Context) {
		clazz.context.getDefinitions()
			.filterIsInstance<OverloadedMethodDefinition>()
			.forEach {
				clazz.context.overwriteMember(
					scanMethodOverloads(it, clazz.context)
				)
			}

		clazz.context.defineMember(
			ResolvedVariableDeclaration(
				identifier = "<this>",
				kind = ResolvedVariableDeclaration.Kind.PSEUDO,
				type = ResolvedType(clazz),
				ordinal = 0
			)
		)
	}

	private fun scanMethodOverloads(
		ovl: OverloadedMethodDefinition,
		context: ClassContext
	): ResolvedOvlMethodDefinition {
		val newOvl = ResolvedOvlMethodDefinition(identifier = ovl.identifier)

		ovl
			.map { scanMethodDefinition(it, context) }
			.forEach { newOvl.addOverload(it) }

		return newOvl
	}

	private fun scanMethodArgDefinition(
		definition: TypedVariableDefinition,
		context: Context,
		ordinal: Int
	) = ResolvedVariableDeclaration(
		identifier = definition.identifier,
		type = scanType(definition.typeNode, context),
		kind = ResolvedVariableDeclaration.Kind.PARAMETER,
		ordinal = ordinal,
		immutable = true
	)

	private fun scanType(type: ClassNameNode, context: Context): ResolvedType {
		require(context.isDefined(type.identifier.identifier)) {
			"Undefined identifier '${type.identifier.identifier}'"
		}

		val classDef = context.getDefinition(type.identifier.identifier)

		require(classDef is ClassDefinition) {
			"'${classDef.identifier}' is not a type"
		}

		require(type.genericParameter == null) { "Generics not supported" }

		return ResolvedType(classDef)
	}

	private fun scanMethodDefinition(definition: MethodDefinition, context: Context): ResolvedMethodDefinition {
		val rettype = if (definition.returnTypeNode != null) {
			scanType(definition.returnTypeNode, context)
		} else {
			null
		}

		val params = definition.params.mapIndexed { idx, it ->
			scanMethodArgDefinition(it, context, idx + 1)
		}

		return ResolvedMethodDefinition(
			identifier = definition.identifier,
			params = params,
			returnType = rettype,
			body = scanBlock(definition.body, params, context)
		)
	}

	private fun scanBlock(
		block: Block,
		params: List<ResolvedVariableDeclaration>,
		context: Context
	): Block {
		params.forEach { block.context.overwriteVariable(it) }

		if (block.context.isDefined("<return>")) {
			val retn = block.context.getDefinition("<return>")

			require(retn is TypedVariableDefinition)

			block.context.overwriteVariable(
				scanMethodArgDefinition(retn, context, -1)
			)
		}

		block.context.getDefinitions()
			.filterIsInstance<InitializerVariableDefinition>()
			.forEach { block.context.overwriteVariable(it) }

		return block
	}
}