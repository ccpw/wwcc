package org.ccpw.wwcc.compiler.semantics

import org.ccpw.wwcc.compiler.ast.*
import org.ccpw.wwcc.compiler.cast.context.BlockContext
import org.ccpw.wwcc.compiler.cast.context.ClassContext
import org.ccpw.wwcc.compiler.cast.context.GlobalContext
import org.ccpw.wwcc.compiler.cast.context.definition.*
import org.ccpw.wwcc.compiler.cast.context.statement.*

object CASTBuilder : Traverser {
	fun process(ast: RootNode) =
		visit(ast)

	override fun visit(node: RootNode): GlobalContext {
		val context = GlobalContext()

		node.classes
			.associateWith {
				val clazz = context.defineClass(it.welcome(this))

				clazz.context.setParentContext(context)

				clazz
			}
			.forEach { (node, clazz) ->
				val parentId = node.parent?.identifier?.identifier ?: return@forEach
				val parent = context.getDefinition(parentId)

				clazz.context.setParentContext(
					parent.context
				)

				clazz.parent = parent
			}

		return context
	}

	override fun visit(node: ClassNode): ClassDefinition {
		val context = ClassContext()

		require(node.name.genericParameter == null) { "Generics are not supported" }

		node.definitions.forEach {
			context
				.defineMember(it.welcome(this))
		}

		context
			.getDefinitions()
			.filterIsInstance<OverloadedMethodDefinition>()
			.forEach { it.map { m -> m.body.context.setParentContext(context) } }

		return ClassDefinition(
			identifier = node.name.identifier.identifier,
			context = context
		)
	}

	override fun visit(node: VariableDeclarationNode): InitializerVariableDefinition {
		return InitializerVariableDefinition(
			identifier = node.identifier.identifier,
			initializerNode = node.initializer
		)
	}

	override fun visit(node: MethodDefinitionNode): MethodDefinition {
		val body = node.body.welcome(this)

		val params = node.parameters.map { it.welcome(this) }

		params.forEach { body.context.defineVariable(it) }

		if (node.returnType != null) {
			body.context.defineVariable(
				TypedVariableDefinition(
					"<return>",
					typeNode = node.returnType
				)
			)
		}

		return MethodDefinition(
			identifier = node.identifier.identifier,
			params = params,
			returnTypeNode = node.returnType,
			body
		)
	}

	override fun visit(node: CtorDefinitionNode): MethodDefinition {
		val body = node.body.welcome(this)

		val params = node.parameters.map { it.welcome(this) }

		params.forEach { body.context.defineVariable(it) }

		return MethodDefinition(
			identifier = CtorDefinition.IDENTIFIER,
			params = params,
			returnTypeNode = null,
			body
		)
	}

	override fun visit(node: MethodParameterNode): TypedVariableDefinition {
		return TypedVariableDefinition(
			identifier = node.identifier.identifier,
			typeNode = node.type
		)
	}

	override fun visit(node: BlockNode): Block {
		val context = BlockContext()
		val firstDef = node.statements.indexOfFirst { it is VariableDeclarationNode }

		if (firstDef < 0) {
			return Block(
				statements = node.statements.map { it.welcome(this) },
				context
			)
		}

		val beforeDecl = node.statements.subList(0, firstDef + 1).map { it.welcome(this) }
		val afterDecl = node.statements.subList(firstDef + 1, node.statements.size)

		val definition = beforeDecl.last() as VariableDefinition // we surely know that it is a vardecl
		context.defineVariable(definition)

		val nestedBlock = BlockNode(afterDecl).welcome(this)

		nestedBlock.context.setParentContext(context)

		return Block(
			statements = beforeDecl + nestedBlock,
			context
		)
	}

	override fun visit(node: AssignmentNode): Statement {
		return Assignment(
			target = node.target.identifier,
			valueNode = node.value
		)
	}

	override fun visit(node: IfClauseNode): Conditional {
		return Conditional(
			conditionNode = node.condition,
			truthyBranch = node.thenBranch.welcome(this),
			falseyBranch = node.elseBranch.welcome(this),
		)
	}

	override fun visit(node: WhileClauseNode): Loop {
		return Loop(
			conditionNode = node.condition,
			body = node.body.welcome(this)
		)
	}

	override fun visit(node: ReturnNode): ReturnStatement {
		return ReturnStatement(
			valueNode = node.value
		)
	}
}