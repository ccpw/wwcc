package org.ccpw.wwcc.compiler.semantics

import org.ccpw.wwcc.compiler.cast.context.Context
import org.ccpw.wwcc.compiler.cast.context.GlobalContext
import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedOvlMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedVariableDeclaration
import org.ccpw.wwcc.compiler.cast.context.expression.Expression
import org.ccpw.wwcc.compiler.cast.context.statement.*

object CASTControlsResolver {
	fun process(cast: GlobalContext) =
		scanGlobal(cast)

	private fun scanGlobal(cast: GlobalContext): GlobalContext {
		cast.getDefinitions()
			.map { scanClass(it) }

		return cast
	}

	private fun scanClass(clazz: ClassDefinition): ClassDefinition {
		clazz.context.getDefinitions()
			.map {
				when (it) {
					is ResolvedOvlMethodDefinition -> scanOverload(it, clazz.context)
					else -> it
				}
			}
			.forEach { clazz.context.overwriteMember(it) }

		return clazz
	}

	private fun scanOverload(
		ovl: ResolvedOvlMethodDefinition,
		context: Context
	): ResolvedOvlMethodDefinition {
		ovl
			.map { scanMethod(it, context) }
			.forEach { ovl.overwriteOverload(it) }

		return ovl
	}

	private fun scanMethod(definition: ResolvedMethodDefinition, context: Context): ResolvedMethodDefinition {
		return ResolvedMethodDefinition(
			identifier = definition.identifier,
			params = definition.params,
			returnType = definition.returnType,
			body = scanBlock(definition.body)
		)
	}

	private fun scanBlock(blk: Block): Block {
		blk.statements = blk.statements.map {
			when (it) {
				is Loop -> scanLoop(it, blk.context)
				is Conditional -> scanConditional(it, blk.context)
				is ReturnStatement -> scanReturn(it, blk.context)
				is Block -> scanBlock(it)
				else -> it
			}
		}

		return blk
	}

	private fun scanReturn(retn: ReturnStatement, context: Context): ResolvedReturn {
		if (!context.isDefined("<return>")) {
			require(retn.valueNode == null) { "Returning value from void function" }

			return ResolvedReturn()
		}

		require(retn.valueNode is Expression) { "Expected expression" }

		val returnDecl = context.getDefinition("<return>")
		require(returnDecl is ResolvedVariableDeclaration)

		val valueType = retn.valueNode.type

		require(valueType.reference.isSubtypeOf(returnDecl.type.reference)) { "Expected to return ${returnDecl.type.reference}" }

		return ResolvedReturn(
			value = retn.valueNode
		)
	}

	private fun scanLoop(loop: Loop, context: Context): ResolvedLoop {
		require(loop.conditionNode is Expression) { "Expected expression" }

		val condType = loop.conditionNode.type

		require(condType.reference.isSubtypeOf(BuiltinTypes.BOOLEAN.reference)) { "Expected 'Boolean'" }


		return ResolvedLoop(
			condition = loop.conditionNode,
			body = loop.body
		)
	}

	private fun scanConditional(cond: Conditional, context: Context): ResolvedIf {
		require(cond.conditionNode is Expression) { "Expected expression" }

		val condType = cond.conditionNode.type

		require(condType.reference.isSubtypeOf(BuiltinTypes.BOOLEAN.reference)) { "Expected 'Boolean'" }


		return ResolvedIf(
			condition = cond.conditionNode,
			thenBranch = cond.truthyBranch,
			elseBranch = cond.falseyBranch
		)
	}
}