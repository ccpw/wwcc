package org.ccpw.wwcc.compiler.semantics

import org.ccpw.wwcc.compiler.ast.*
import org.ccpw.wwcc.compiler.cast.context.Context
import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedOvlMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedVariableDeclaration
import org.ccpw.wwcc.compiler.cast.context.expression.*

class ExpressionResolver(
	val context: Context
) {
	private val typifier = ExpressionTypeResolver(context)

	fun resolve(expression: ExpressionNode): Expression =
		when (expression) {
			is LiteralNode -> resolveLiteral(expression)
			is IdentifierNode -> resolveVariable(expression)
			is ThisNode -> resolveThis()
			is CtorInvocationNode -> resolveCtorInvocation(expression)
			is MemberInvocationNode -> resolveMemberInvocation(expression)
			else -> error("Invalid expression")
		}

	private fun resolveCtorInvocation(expression: CtorInvocationNode): CtorInvocation {
		val className = expression.targetClass.identifier.identifier
		val clazz = context.getDefinition(className)

		require(clazz is ClassDefinition) { "'$className' class is not defined" }

		val ctorOvl = clazz.context.getDefinition("<init>")

		require(ctorOvl is ResolvedOvlMethodDefinition) { "'$className' does not provide a constructor" }

		val args = resolveCtorMethodInvocationArgs(expression)

		val ctor = ctorOvl.getOverload(
			args.map { it.type.reference }
		)

		return CtorInvocation(
			target = clazz,
			definition = ctor,
			args = args,
			type = typifier.resolve(expression)
		)
	}

	private fun resolveCtorMethodInvocationArgs(expression: CtorInvocationNode) =
		expression.arguments.map { resolve(it) }

	private fun resolveMemberInvocation(expression: MemberInvocationNode): MemberInvocation {
		val targetClass = typifier.resolve(expression.target).reference

		val definition = targetClass.context.getDefinition(expression.member.identifier)

		return if (expression.arguments.isNotEmpty()) {
			require(definition is ResolvedOvlMethodDefinition) { "'${definition.identifier}' is not a function" }

			val ovl = definition.getOverload(
				expression.arguments.map { resolve(it).type.reference }
			)

			resolveMethodInvocation(ovl, expression)
		} else {
			if (definition is ResolvedMethodDefinition) {
				resolveMethodInvocation(definition, expression)
			} else {
				require(definition is ResolvedVariableDeclaration) {
					"'${definition.identifier}' is neither a function or a field"
				}

				resolveFieldInvocation(definition, expression)
			}
		}
	}

	private fun resolveFieldInvocation(
		definition: ResolvedVariableDeclaration,
		expression: MemberInvocationNode
	) = FieldInvocation(
		target = resolve(expression.target),
		targetType = typifier.resolve(expression.target),
		field = definition,
		type = typifier.resolve(expression)
	)

	private fun resolveMethodInvocation(
		resolved: ResolvedMethodDefinition,
		expression: MemberInvocationNode
	) = MethodInvocation(
		target = resolve(expression.target),
		targetType = typifier.resolve(expression.target),
		method = resolved,
		args = expression.arguments.map { resolve(it) },
		type = typifier.resolve(expression)
	)

	private fun resolveThis(): ResolvedVariableDeclaration {
		val definition = context.getDefinition("<this>")

		require(definition is ResolvedVariableDeclaration) { "'this' is not a variable" }

		return definition
	}

	private fun resolveVariable(identifier: IdentifierNode): ResolvedVariableDeclaration {
		val definition = context.getDefinition(identifier.identifier)

		require(definition is ResolvedVariableDeclaration) { "${identifier.identifier} is not a variable" }

		return definition
	}

	// private

	private fun resolveLiteral(literal: LiteralNode) =
		when (literal) {
			is IntLiteralNode -> IntLiteral(
				literal.value,
				type = typifier.resolve(literal)
			)
			is FloatLiteralNode -> FloatLiteral(
				literal.value,
				type = typifier.resolve(literal)
			)
			is BoolLiteralNode -> BoolLiteral(
				literal.value,
				type = typifier.resolve(literal)
			)
			else -> error("Invalid literal")
		}
}