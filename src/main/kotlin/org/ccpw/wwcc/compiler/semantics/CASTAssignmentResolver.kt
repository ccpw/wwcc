package org.ccpw.wwcc.compiler.semantics

import org.ccpw.wwcc.compiler.cast.context.Context
import org.ccpw.wwcc.compiler.cast.context.GlobalContext
import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedOvlMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedVariableDeclaration
import org.ccpw.wwcc.compiler.cast.context.expression.Expression
import org.ccpw.wwcc.compiler.cast.context.statement.Assignment
import org.ccpw.wwcc.compiler.cast.context.statement.Block
import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedAssignment

object CASTAssignmentResolver {
	fun process(cast: GlobalContext) =
		scanGlobal(cast)

	private fun scanGlobal(cast: GlobalContext): GlobalContext {
		cast.getDefinitions()
			.map { scanClass(it, cast) }

		return cast
	}

	private fun scanClass(clazz: ClassDefinition, context: Context): ClassDefinition {
		clazz.context.getDefinitions()
			.map {
				when (it) {
					is ResolvedOvlMethodDefinition -> scanOverload(it, clazz.context)
					else -> it
				}
			}
			.forEach { clazz.context.overwriteMember(it) }

		return clazz
	}

	private fun scanOverload(
		ovl: ResolvedOvlMethodDefinition,
		context: Context
	): ResolvedOvlMethodDefinition {
		ovl
			.map { scanMethod(it, context) }
			.forEach { ovl.overwriteOverload(it) }

		return ovl
	}

	private fun scanMethod(definition: ResolvedMethodDefinition, context: Context): ResolvedMethodDefinition {
		return ResolvedMethodDefinition(
			identifier = definition.identifier,
			params = definition.params,
			returnType = definition.returnType,
			body = scanBlock(definition.body)
		)
	}

	private fun scanBlock(blk: Block): Block {
		blk.statements = blk.statements.map {
			when (it) {
				is Assignment -> scanAssignment(it, blk.context)
				is Block -> scanBlock(it)
				else -> it
			}
		}

		return blk
	}

	private fun scanAssignment(asmt: Assignment, context: Context): ResolvedAssignment {
		val resolved = context.getDefinition(asmt.target)

		require(resolved is ResolvedVariableDeclaration) { "'${resolved.identifier}' is not a variable" }
		require(
			listOf(
				ResolvedVariableDeclaration.Kind.FIELD,
				ResolvedVariableDeclaration.Kind.VARIABLE
			).contains(resolved.kind)
		) { "'${resolved.identifier}' is readonly" }

		return ResolvedAssignment(resolved, asmt.valueNode as Expression)
	}
}