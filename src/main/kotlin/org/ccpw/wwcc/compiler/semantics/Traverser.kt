package org.ccpw.wwcc.compiler.semantics

import org.ccpw.wwcc.compiler.ast.*
import org.ccpw.wwcc.compiler.cast.context.GlobalContext
import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.InitializerVariableDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.MethodDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.TypedVariableDefinition
import org.ccpw.wwcc.compiler.cast.context.statement.*

interface Traverser {
	fun visit(node: RootNode): GlobalContext
	fun visit(node: ClassNode): ClassDefinition
	fun visit(node: VariableDeclarationNode): InitializerVariableDefinition
	fun visit(node: MethodDefinitionNode): MethodDefinition
	fun visit(node: CtorDefinitionNode): MethodDefinition
	fun visit(node: MethodParameterNode): TypedVariableDefinition
	fun visit(node: BlockNode): Block
	fun visit(node: AssignmentNode): Statement
	fun visit(node: IfClauseNode): Conditional
	fun visit(node: WhileClauseNode): Loop
	fun visit(node: ReturnNode): ReturnStatement
}