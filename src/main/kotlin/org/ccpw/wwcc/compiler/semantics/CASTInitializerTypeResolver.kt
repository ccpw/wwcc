package org.ccpw.wwcc.compiler.semantics

import org.ccpw.wwcc.compiler.cast.context.BlockContext
import org.ccpw.wwcc.compiler.cast.context.Context
import org.ccpw.wwcc.compiler.cast.context.GlobalContext
import org.ccpw.wwcc.compiler.cast.context.definition.*
import org.ccpw.wwcc.compiler.cast.context.statement.Assignment
import org.ccpw.wwcc.compiler.cast.context.statement.Block

object CASTInitializerTypeResolver {
	fun process(cast: GlobalContext) =
		scanGlobal(cast)

	private fun scanGlobal(cast: GlobalContext): GlobalContext {
		cast.getDefinitions()
			.forEach { scanClass(it) }

		return cast
	}

	private fun scanClass(clazz: ClassDefinition): ClassDefinition {
		val initializers = mutableListOf<Assignment>()

		clazz.context.getDefinitions()
			.filterIsInstance<InitializerVariableDefinition>()
			.map {
				val (decl, asmt) = scanInitializerDefinition(
					it, clazz.context,
					kind = ResolvedVariableDeclaration.Kind.FIELD,
					ordinal = -1
				)

				initializers.add(asmt)

				decl
			}
			.forEach { clazz.context.overwriteMember(it) }

		clazz.context.getDefinitions()
			.filterIsInstance<ResolvedOvlMethodDefinition>()
			.map { scanOverload(it, clazz.context) }
			.forEach { clazz.context.overwriteMember(it) }

		clazz.context.defineMember(
			createPreInitMethod(
				initializers, clazz.context,
				asInit = !clazz.context.isDefined("<init>")
			)
		)

		return clazz
	}

	private fun scanOverload(
		ovl: ResolvedOvlMethodDefinition,
		context: Context
	): ResolvedOvlMethodDefinition {
		ovl
			.map { scanMethod(it, context) }
			.forEach { ovl.overwriteOverload(it) }

		return ovl
	}

	private fun scanMethod(varDef: ResolvedMethodDefinition, context: Context) =
		ResolvedMethodDefinition(
			identifier = varDef.identifier,
			params = varDef.params,
			body = scanBlock(varDef.body, context, varDef.params.size + 1),
			returnType = varDef.returnType
		)

	private fun scanBlock(blk: Block, context: Context, initOrd: Int): Block {
		var ordinarity = initOrd

		blk.statements = blk.statements.flatMap {
			when (it) {
				is InitializerVariableDefinition -> {
					val (decl, asmt) = scanInitializerDefinition(
						it, blk.context,
						kind = ResolvedVariableDeclaration.Kind.VARIABLE,
						ordinal = ordinarity
					)

					ordinarity += 1

					blk.context.overwriteVariable(decl)

					listOf(decl, asmt)
				}
				is Block -> listOf(scanBlock(it, blk.context, ordinarity))
				else -> listOf(it)
			}
		}

		return blk
	}

	private fun createPreInitMethod(
		initializers: List<Assignment>,
		context: Context,
		asInit: Boolean = false
	): ResolvedOvlMethodDefinition {
		val blkContext = BlockContext()
		blkContext.setParentContext(context)

		val meth = ResolvedMethodDefinition(
			identifier = if (asInit) "<init>" else "<preinit>",
			params = listOf(),
			returnType = null,
			body = Block(initializers, context = blkContext)
		)

		val ovl = ResolvedOvlMethodDefinition(identifier = "<preinit>")
		ovl.addOverload(meth)

		return ovl
	}

	private fun scanInitializerDefinition(
		definition: InitializerVariableDefinition,
		context: Context,
		kind: ResolvedVariableDeclaration.Kind,
		ordinal: Int
	) = Pair(
		varDeclFromDefinition(definition, context, kind, ordinal),
		assignmentFromDefinition(definition)
	)

	private fun varDeclFromDefinition(
		definition: InitializerVariableDefinition,
		context: Context,
		kind: ResolvedVariableDeclaration.Kind,
		ordinarity: Int
	): ResolvedVariableDeclaration {
		val type = ExpressionTypeResolver(context).resolve(definition.initializerNode)

		return ResolvedVariableDeclaration(
			identifier = definition.identifier,
			kind = kind,
			type = type,
			ordinal = ordinarity
		)
	}

	private fun assignmentFromDefinition(
		definition: InitializerVariableDefinition
	) =
		Assignment(
			target = definition.identifier,
			valueNode = definition.initializerNode
		)
}