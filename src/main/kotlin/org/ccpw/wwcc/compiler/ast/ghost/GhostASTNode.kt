package org.ccpw.wwcc.compiler.ast.ghost

import org.apache.commons.collections4.ListUtils
import org.ccpw.wwcc.compiler.lexer.lexeme.PositionedLexeme

data class GhostASTNode(
	val nodeType: GhostASTNodeType = GhostASTNodeType.NULL,
	val value: PositionedLexeme? = null,
	val children: List<GhostASTNode> = emptyList()
) {
	constructor(type: GhostASTNodeType, lexeme: PositionedLexeme) : this(type, lexeme, emptyList())

	private fun indent(level: Int): String {
		return "  ".repeat(level)
	}

	fun inspect(level: Int = 0): String {
		val sb = StringBuilder(indent(level) + "[GhostNode $nodeType]\n")

		if (value != null) {
			sb.append(indent(level + 1) + "[value] = $value\n")
		}

		if (children.isNotEmpty()) sb.append(indent(level + 1) + "[children]\n")
		children.forEach { node ->
			sb.append(node.inspect(level + 2))
		}

		return sb.toString()
	}

	fun addChild(child: GhostASTNode): GhostASTNode {
		return GhostASTNode(
			nodeType,
			value,
			children = ListUtils.union(children, listOf(child))
		)
	}
}