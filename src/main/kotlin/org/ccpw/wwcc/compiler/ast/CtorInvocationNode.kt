package org.ccpw.wwcc.compiler.ast

import kotlinx.serialization.Serializable

@Serializable
data class CtorInvocationNode(
	val targetClass: ClassNameNode,
	val arguments: List<ExpressionNode>
) : ExpressionNode