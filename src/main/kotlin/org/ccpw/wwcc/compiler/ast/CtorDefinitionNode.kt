package org.ccpw.wwcc.compiler.ast

import kotlinx.serialization.Serializable
import org.ccpw.wwcc.compiler.semantics.Traverser

@Serializable
data class CtorDefinitionNode(
	val parameters: List<MethodParameterNode>,
	val body: BlockNode
) : DefinitionNode {
	override fun welcome(visitor: Traverser) = visitor.visit(this)
}