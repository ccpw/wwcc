package org.ccpw.wwcc.compiler.ast

import kotlinx.serialization.Serializable

@Serializable
data class ClassNameNode(
	val identifier: IdentifierNode,
	val genericParameter: ClassNameNode? = null
) : ASTNode {
	override fun toString(): String {
		if (genericParameter != null) return "${identifier.identifier}__$genericParameter"

		return identifier.identifier
	}
}