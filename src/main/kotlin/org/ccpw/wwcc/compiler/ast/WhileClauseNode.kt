package org.ccpw.wwcc.compiler.ast

import kotlinx.serialization.Serializable
import org.ccpw.wwcc.compiler.semantics.Traverser

@Serializable
data class WhileClauseNode(
	val condition: ExpressionNode,
	val body: BlockNode
) : StatementNode {
	override fun welcome(visitor: Traverser) =
		visitor.visit(this)
}