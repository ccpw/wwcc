package org.ccpw.wwcc.compiler.ast

import org.ccpw.wwcc.compiler.cast.context.definition.MemberDefinition
import org.ccpw.wwcc.compiler.semantics.Traverser

interface DefinitionNode : ASTNode {
	override fun welcome(visitor: Traverser): MemberDefinition
}
