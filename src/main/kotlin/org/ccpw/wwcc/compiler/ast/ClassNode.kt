package org.ccpw.wwcc.compiler.ast

import kotlinx.serialization.Serializable
import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.semantics.Traverser

@Serializable
data class ClassNode(
	val name: ClassNameNode,
	val parent: ClassNameNode? = null,
	val definitions: List<DefinitionNode>
) : ASTNode {
	override fun welcome(visitor: Traverser): ClassDefinition =
		visitor.visit(this)
}