package org.ccpw.wwcc.compiler.ast

import org.ccpw.wwcc.compiler.cast.context.statement.Statement
import org.ccpw.wwcc.compiler.semantics.Traverser

interface StatementNode : ASTNode {
	override fun welcome(visitor: Traverser): Statement
}
