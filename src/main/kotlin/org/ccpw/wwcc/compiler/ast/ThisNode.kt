package org.ccpw.wwcc.compiler.ast

import kotlinx.serialization.Serializable

@Serializable
class ThisNode : ExpressionNode