package org.ccpw.wwcc.compiler.ast

import kotlinx.serialization.Serializable

@Serializable
data class MemberInvocationNode(
	val target: ExpressionNode,
	val member: IdentifierNode,
	val arguments: List<ExpressionNode>
) : ExpressionNode