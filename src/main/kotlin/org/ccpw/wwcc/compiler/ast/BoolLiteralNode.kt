package org.ccpw.wwcc.compiler.ast

import kotlinx.serialization.Serializable

@Serializable
data class BoolLiteralNode(
	val value: Boolean
) : LiteralNode