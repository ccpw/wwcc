package org.ccpw.wwcc.compiler.ast

import kotlinx.serialization.Serializable

@Serializable
data class IntLiteralNode(
	val value: Int
) : LiteralNode