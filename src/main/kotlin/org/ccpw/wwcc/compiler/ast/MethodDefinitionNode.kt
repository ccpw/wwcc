package org.ccpw.wwcc.compiler.ast

import kotlinx.serialization.Serializable
import org.ccpw.wwcc.compiler.semantics.Traverser

@Serializable
data class MethodDefinitionNode(
	val identifier: IdentifierNode,
	val parameters: List<MethodParameterNode>,
	val returnType: ClassNameNode?,
	val body: BlockNode
) : DefinitionNode {
	override fun welcome(visitor: Traverser) = visitor.visit(this)
}