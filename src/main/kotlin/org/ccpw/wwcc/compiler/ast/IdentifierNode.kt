package org.ccpw.wwcc.compiler.ast

import kotlinx.serialization.Serializable

@Serializable
data class IdentifierNode(
	val identifier: String
) : ExpressionNode