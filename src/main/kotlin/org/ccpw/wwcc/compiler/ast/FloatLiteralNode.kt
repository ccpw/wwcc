package org.ccpw.wwcc.compiler.ast

import kotlinx.serialization.Serializable

@Serializable
data class FloatLiteralNode(
	val value: Double
) : LiteralNode