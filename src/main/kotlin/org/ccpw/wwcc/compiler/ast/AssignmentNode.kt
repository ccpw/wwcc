package org.ccpw.wwcc.compiler.ast

import kotlinx.serialization.Serializable
import org.ccpw.wwcc.compiler.semantics.Traverser

@Serializable
data class AssignmentNode(
	val target: IdentifierNode,
	val value: ExpressionNode
) : StatementNode {
	override fun welcome(visitor: Traverser) =
		visitor.visit(this)
}