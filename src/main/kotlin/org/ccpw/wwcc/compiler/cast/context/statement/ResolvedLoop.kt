package org.ccpw.wwcc.compiler.cast.context.statement

import org.ccpw.wwcc.compiler.cast.context.expression.Expression

class ResolvedLoop(
	val condition: Expression,
	val body: Block
) : Statement