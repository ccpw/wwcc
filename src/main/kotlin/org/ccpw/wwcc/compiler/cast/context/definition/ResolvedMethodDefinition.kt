package org.ccpw.wwcc.compiler.cast.context.definition

import org.ccpw.wwcc.compiler.cast.context.statement.Block
import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedType

data class ResolvedMethodDefinition(
	override val identifier: String,
	val params: List<ResolvedVariableDeclaration>,
	val returnType: ResolvedType?,
	val body: Block,
) : MemberDefinition {
	fun paramsDigest() =
		params.joinToString(";") { it.type.reference.identifier }
}