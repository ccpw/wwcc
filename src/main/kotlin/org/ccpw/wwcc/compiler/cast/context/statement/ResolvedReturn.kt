package org.ccpw.wwcc.compiler.cast.context.statement

import org.ccpw.wwcc.compiler.cast.context.expression.Expression

data class ResolvedReturn(
	val value: Expression? = null
) : Statement
