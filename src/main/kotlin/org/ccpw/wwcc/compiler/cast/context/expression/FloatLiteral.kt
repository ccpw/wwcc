package org.ccpw.wwcc.compiler.cast.context.expression

import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedType

data class FloatLiteral(
	val value: Double,
	override val type: ResolvedType,
) : Literal