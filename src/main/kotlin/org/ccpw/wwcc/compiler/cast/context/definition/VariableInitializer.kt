package org.ccpw.wwcc.compiler.cast.context.definition

import org.ccpw.wwcc.compiler.cast.context.statement.Block

data class VariableInitializer(
	override val identifier: String,
	val body: Block
) : VariableDefinition