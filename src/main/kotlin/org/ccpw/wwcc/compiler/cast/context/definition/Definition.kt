package org.ccpw.wwcc.compiler.cast.context.definition

import org.ccpw.wwcc.compiler.cast.CASTNode

interface Definition : CASTNode {
	val identifier: String
}