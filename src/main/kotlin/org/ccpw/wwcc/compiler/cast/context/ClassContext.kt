package org.ccpw.wwcc.compiler.cast.context

import org.ccpw.wwcc.compiler.cast.context.definition.MemberDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.MethodDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.OverloadedMethodDefinition

class ClassContext : CommonContext() {
	private val definitionMap = mutableMapOf<String, MemberDefinition>()

	override fun isDefinedLocally(identifier: String) =
		definitionMap.containsKey(identifier)

	override fun getLocalDefinition(identifier: String) = definitionMap[identifier]

	override fun getDefinitions(): List<MemberDefinition> = definitionMap.values.toList()

	fun defineMember(definition: MemberDefinition): MemberDefinition {
		if (definition is MethodDefinition) return defineMethodOverload(definition)

		require(!isDefinedLocally(definition.identifier)) { "Identifier is already defined" }

		definitionMap[definition.identifier] = definition

		return definition
	}

	private fun defineMethodOverload(definition: MethodDefinition): MemberDefinition {
		if (!isDefinedLocally(definition.identifier)) {
			definitionMap[definition.identifier] = OverloadedMethodDefinition(definition.identifier)
		}

		val ovl = definitionMap[definition.identifier]

		require(ovl is OverloadedMethodDefinition) { "Invalid overload" }

		ovl.addOverload(definition)

		return ovl
	}

	override fun toString(): String {
		return "ClassContext($definitionMap)"
	}

	fun overwriteMember(definition: MemberDefinition) {
		definitionMap[definition.identifier] = definition
	}
}