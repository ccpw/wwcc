package org.ccpw.wwcc.compiler.cast.context.statement

import org.ccpw.wwcc.compiler.ast.ExpressionNode

class Loop(
	val conditionNode: ExpressionNode,
	val body: Block
) : Statement