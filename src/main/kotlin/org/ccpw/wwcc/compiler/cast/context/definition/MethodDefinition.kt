package org.ccpw.wwcc.compiler.cast.context.definition

import org.ccpw.wwcc.compiler.ast.ClassNameNode
import org.ccpw.wwcc.compiler.cast.context.statement.Block

data class MethodDefinition(
	override val identifier: String,
	val params: List<TypedVariableDefinition>,
	val returnTypeNode: ClassNameNode?,
	val body: Block
) : MemberDefinition {
	fun paramsDigest() =
		params.joinToString(";") { it.typeNode.toString() }
}