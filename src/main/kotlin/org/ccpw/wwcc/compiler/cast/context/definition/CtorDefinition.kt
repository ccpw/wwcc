package org.ccpw.wwcc.compiler.cast.context.definition

import org.ccpw.wwcc.compiler.ast.CtorDefinitionNode

data class CtorDefinition(
	val node: CtorDefinitionNode,
) : MemberDefinition {
	companion object {
		const val IDENTIFIER = "<init>"
	}

	override val identifier: String = IDENTIFIER
}