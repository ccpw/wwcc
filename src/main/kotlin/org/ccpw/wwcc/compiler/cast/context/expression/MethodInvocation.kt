package org.ccpw.wwcc.compiler.cast.context.expression

import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedType

data class MethodInvocation(
	val target: Expression,
	val targetType: ResolvedType,
	val method: ResolvedMethodDefinition,
	val args: List<Expression>,
	override val type: ResolvedType
) : MemberInvocation