package org.ccpw.wwcc.compiler.cast.context.expression

import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition
import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedMethodDefinition
import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedType

data class CtorInvocation(
	val target: ClassDefinition,
	val definition: ResolvedMethodDefinition,
	val args: List<Expression>,
	override val type: ResolvedType
) : Expression