package org.ccpw.wwcc.compiler.cast.context.expression

import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedType

data class IntLiteral(
	internal val value: Int,
	override val type: ResolvedType
) : Literal