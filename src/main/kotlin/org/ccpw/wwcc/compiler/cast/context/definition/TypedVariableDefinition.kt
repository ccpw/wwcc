package org.ccpw.wwcc.compiler.cast.context.definition

import org.ccpw.wwcc.compiler.ast.ClassNameNode

data class TypedVariableDefinition(
	override val identifier: String,
	val typeNode: ClassNameNode
) : VariableDefinition