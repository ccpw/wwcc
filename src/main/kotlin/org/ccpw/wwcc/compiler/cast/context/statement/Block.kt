package org.ccpw.wwcc.compiler.cast.context.statement

import org.ccpw.wwcc.compiler.cast.context.BlockContext

data class Block(
	var statements: List<Statement>,
	val context: BlockContext
) : Statement