package org.ccpw.wwcc.compiler.cast.context

import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition

class GlobalContext : Context, Cloneable {
	private val classMap = mutableMapOf<String, ClassDefinition>()

	override fun isDefined(identifier: String): Boolean {
		return classMap.containsKey(identifier)
	}

	override fun getDefinition(identifier: String): ClassDefinition {
		return checkNotNull(classMap[identifier]) { "Undefined identifier '$identifier'" }
	}

	override fun setParentContext(context: Context) = this

	override fun getParentContext(): Context? = null

	fun defineClass(definition: ClassDefinition): ClassDefinition {
		check(!isDefined(definition.identifier)) { "Duplicate definition of an identifier '${definition.identifier}'" }

		classMap[definition.identifier] = definition

		return definition
	}

	fun dumpReset(): GlobalContext {
		val current = GlobalContext()

		getDefinitions().forEach { current.defineClass(it) }

		classMap.clear()

		return current
	}

	override fun getDefinitions(): List<ClassDefinition> {
		return classMap.values.toList()
	}

	override fun toString(): String {
		return "GlobalContext($classMap)"
	}
}