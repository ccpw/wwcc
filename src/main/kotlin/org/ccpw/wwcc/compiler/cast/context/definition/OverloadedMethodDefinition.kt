package org.ccpw.wwcc.compiler.cast.context.definition

open class OverloadedMethodDefinition(
	override val identifier: String
) : MemberDefinition {
	private val methodMap = mutableMapOf<String, MethodDefinition>()

	fun addOverload(definition: MethodDefinition) {
		require(!hasOverload(definition.paramsDigest())) { "Method already defined with same signature" }

		methodMap[definition.paramsDigest()] = definition
	}

	private fun hasOverload(digest: String) =
		methodMap.containsKey(digest)

	fun <R> map(block: (meth: MethodDefinition) -> R) =
		methodMap.values.map(block)

	fun find(block: (meth: MethodDefinition) -> Boolean) =
		checkNotNull(methodMap.values.find(block)) {
			"No overload with given signature"
		}
}