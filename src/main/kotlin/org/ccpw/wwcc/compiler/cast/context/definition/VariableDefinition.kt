package org.ccpw.wwcc.compiler.cast.context.definition

import org.ccpw.wwcc.compiler.cast.context.statement.Statement

interface VariableDefinition : MemberDefinition, Statement