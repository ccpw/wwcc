package org.ccpw.wwcc.compiler.cast.context.definition

import org.ccpw.wwcc.compiler.cast.context.ClassContext

data class ClassDefinition(
	override val identifier: String,
	val context: ClassContext,
	var parent: ClassDefinition? = null
) : Definition {
	fun isSubtypeOf(other: ClassDefinition): Boolean {
		if (this == other) return true

		return this.parent?.isSubtypeOf(other) ?: false
	}
}