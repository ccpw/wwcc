package org.ccpw.wwcc.compiler.cast.context.expression

import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedType

data class BoolLiteral(
	val value: Boolean,
	override val type: ResolvedType
) : Literal