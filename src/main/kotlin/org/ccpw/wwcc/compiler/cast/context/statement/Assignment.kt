package org.ccpw.wwcc.compiler.cast.context.statement

import org.ccpw.wwcc.compiler.ast.ExpressionNode

data class Assignment(
	val target: String,
	val valueNode: ExpressionNode
) : Statement