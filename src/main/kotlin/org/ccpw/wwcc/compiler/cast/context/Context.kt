package org.ccpw.wwcc.compiler.cast.context

import org.ccpw.wwcc.compiler.cast.CASTNode
import org.ccpw.wwcc.compiler.cast.context.definition.Definition

interface Context : CASTNode {
	fun isDefined(identifier: String): Boolean

	fun getDefinition(identifier: String): Definition

	fun getParentContext(): Context?
	fun setParentContext(context: Context): Context

	fun getDefinitions(): List<Definition>
}