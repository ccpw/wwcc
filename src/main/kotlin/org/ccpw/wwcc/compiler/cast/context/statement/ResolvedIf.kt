package org.ccpw.wwcc.compiler.cast.context.statement

import org.ccpw.wwcc.compiler.cast.context.expression.Expression

data class ResolvedIf(
	val condition: Expression,
	val thenBranch: Block,
	val elseBranch: Block
) : Statement