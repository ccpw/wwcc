package org.ccpw.wwcc.compiler.cast.context

import org.ccpw.wwcc.compiler.cast.context.definition.VariableDefinition

class BlockContext : CommonContext() {
	private val definitionMap = mutableMapOf<String, VariableDefinition>()

	override fun isDefinedLocally(identifier: String) =
		definitionMap.containsKey(identifier)

	override fun getLocalDefinition(identifier: String) = definitionMap[identifier]

	override fun getDefinitions(): List<VariableDefinition> = definitionMap.values.toList()

	fun <T : VariableDefinition> defineVariable(definition: T): T {
		require(!isDefinedLocally(definition.identifier)) { "Identifier already defined" }

		definitionMap[definition.identifier] = definition

		return definition
	}

	override fun toString(): String {
		return "BlockContext($definitionMap)"
	}

	fun overwriteVariable(it: VariableDefinition) {
		definitionMap[it.identifier] = it
	}
}