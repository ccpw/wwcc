package org.ccpw.wwcc.compiler.cast.context.statement

import org.ccpw.wwcc.compiler.ast.ExpressionNode

data class Conditional(
	val conditionNode: ExpressionNode,
	val truthyBranch: Block,
	val falseyBranch: Block
) : Statement