package org.ccpw.wwcc.compiler.cast.context.statement

import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedVariableDeclaration
import org.ccpw.wwcc.compiler.cast.context.expression.Expression

data class ResolvedAssignment(
	val target: ResolvedVariableDeclaration,
	val value: Expression
) : Statement