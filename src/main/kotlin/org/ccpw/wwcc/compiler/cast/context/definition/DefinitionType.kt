package org.ccpw.wwcc.compiler.cast.context.definition

enum class DefinitionType {
	CLASS, CTOR, METHOD, VARIABLE
}