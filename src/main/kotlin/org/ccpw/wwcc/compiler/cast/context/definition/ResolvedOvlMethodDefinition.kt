package org.ccpw.wwcc.compiler.cast.context.definition

class ResolvedOvlMethodDefinition(
	override val identifier: String
) : MemberDefinition {
	private val methodMap = mutableMapOf<String, ResolvedMethodDefinition>()

	fun addOverload(definition: ResolvedMethodDefinition) {
		require(!hasOverload(definition.paramsDigest())) { "Method already defined with same signature" }

		require(!isAmbiguous(definition)) { "Ambiguous method overload" }

		methodMap[definition.paramsDigest()] = definition
	}

	fun overwriteOverload(definition: ResolvedMethodDefinition) {
		methodMap[definition.paramsDigest()] = definition
	}

	private fun isAmbiguous(definition: ResolvedMethodDefinition) =
		hasOverload(definition.params.map { it.type.reference })

	fun hasOverload(argTypes: List<ClassDefinition>) =
		getOverloadNull(argTypes) != null

	fun getOverload(argTypes: List<ClassDefinition>) =
		checkNotNull(getOverloadNull(argTypes)) {
			"No method overload for given parameter types"
		}

	private fun getOverloadNull(argTypes: List<ClassDefinition>): ResolvedMethodDefinition? {
		return findNull { def ->
			if (def.params.size != argTypes.size) return@findNull false

			val verifier = def.params
				.map { it.type.reference }
				.zip(argTypes)

			val ltr = verifier
				.fold(true) { current, (first, second) ->
					current &&
							first.isSubtypeOf(second)
				}
			val rtl = verifier
				.fold(true) { current, (first, second) ->
					current &&
							second.isSubtypeOf(first)
				}


			ltr || rtl
		}
	}

	private fun hasOverload(digest: String) =
		methodMap.containsKey(digest)

	fun <R> map(block: (meth: ResolvedMethodDefinition) -> R) =
		methodMap.values.map(block)

	fun getOverloads() = methodMap.values.toList()

	fun find(block: (meth: ResolvedMethodDefinition) -> Boolean) =
		checkNotNull(findNull(block)) {
			"No overload with given signature"
		}

	private fun findNull(block: (meth: ResolvedMethodDefinition) -> Boolean) =
		methodMap.values.find(block)
}