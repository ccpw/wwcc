package org.ccpw.wwcc.compiler.cast.context.statement

import org.ccpw.wwcc.compiler.cast.CASTNode
import org.ccpw.wwcc.compiler.cast.context.definition.ClassDefinition

class ResolvedType(
	val reference: ClassDefinition,
	val genericArgument: ResolvedType? = null
) : CASTNode {
	override fun toString(): String {
		return if (genericArgument != null)
			"${reference.identifier}[$genericArgument]"
		else
			reference.identifier
	}
}