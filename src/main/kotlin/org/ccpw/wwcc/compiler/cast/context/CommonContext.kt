package org.ccpw.wwcc.compiler.cast.context

import org.ccpw.wwcc.compiler.cast.context.definition.Definition

abstract class CommonContext : Context {
	private var parent: Context? = null

	abstract fun isDefinedLocally(identifier: String): Boolean

	override fun isDefined(identifier: String) =
		isDefinedLocally(identifier) || (parent?.isDefined(identifier) ?: false)

	abstract fun getLocalDefinition(identifier: String): Definition?

	override fun getDefinition(identifier: String): Definition {
		val definition = getLocalDefinition(identifier) ?: parent?.getDefinition(identifier)

		return checkNotNull(definition) { "Undefined identifier '$identifier'" }
	}

	override fun setParentContext(context: Context): Context {
		parent = context

		return this
	}

	override fun getParentContext(): Context? {
		return parent
	}
}