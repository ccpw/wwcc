package org.ccpw.wwcc.compiler.cast.context.expression

import org.ccpw.wwcc.compiler.cast.context.definition.ResolvedVariableDeclaration
import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedType

data class FieldInvocation(
	val target: Expression,
	val field: ResolvedVariableDeclaration,
	val targetType: ResolvedType,
	override val type: ResolvedType,
) : MemberInvocation