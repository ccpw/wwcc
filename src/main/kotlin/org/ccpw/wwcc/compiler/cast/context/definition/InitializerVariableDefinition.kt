package org.ccpw.wwcc.compiler.cast.context.definition

import org.ccpw.wwcc.compiler.ast.ExpressionNode

data class InitializerVariableDefinition(
	override val identifier: String,
	val initializerNode: ExpressionNode,
) : VariableDefinition