package org.ccpw.wwcc.compiler.cast.context.definition

import org.ccpw.wwcc.compiler.cast.context.expression.Expression
import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedType

data class ResolvedVariableDeclaration(
	override val identifier: String,
	val kind: Kind,
	override val type: ResolvedType,
	val immutable: Boolean = false,
	val ordinal: Int
) : VariableDefinition, Expression {
	enum class Kind {
		FIELD, PARAMETER, VARIABLE, PSEUDO
	}
}