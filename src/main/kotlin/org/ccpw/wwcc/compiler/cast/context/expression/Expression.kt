package org.ccpw.wwcc.compiler.cast.context.expression

import org.ccpw.wwcc.compiler.ast.ExpressionNode
import org.ccpw.wwcc.compiler.cast.CASTNode
import org.ccpw.wwcc.compiler.cast.context.statement.ResolvedType

interface Expression : CASTNode, ExpressionNode {
	val type: ResolvedType
}