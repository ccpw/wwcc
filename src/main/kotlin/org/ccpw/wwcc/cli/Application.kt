package org.ccpw.wwcc.cli

import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.Options
import org.ccpw.wwcc.Runner
import kotlin.system.exitProcess

object Application {
	private fun options(): Options {
		val options = Options()

		options.addOption(
			"o", "output-file", true, "Source file"
		)
		options.addOption(
			"v", "verbose", false, "Show more information about errors"
		)
		options.addOption(
			"L", "lexer-only", false, "Perform only lexical analysis"
		)

		return options
	}

	private fun parseOptions(args: Array<String>): RuntimeOptions {
		val parser = DefaultParser()
		val cmd = parser.parse(options(), args)

		val inputs = cmd.argList

		require(inputs.isNotEmpty()) { "error: No inputs provided" }
		require(inputs.size <= 1) { "error: Multiple input files are not allowed" }

		val inputFile = inputs.first()

		return RuntimeOptions(
			lexerOnly = cmd.hasOption("lexer-only"),
			verbose = cmd.hasOption("verbose"),
			outputFile = cmd.getOptionValue("output-file", null),
			inputFile = inputFile,
		)
	}

	private fun openOutputStream() {
	}

	@JvmStatic
	fun main(args: Array<String>) {
		val options: RuntimeOptions

		try {
			options = parseOptions(args)
		} catch (e: IllegalArgumentException) {
			System.err.println(e.message)
			exitProcess(-1)
		}

		try {
			Runner.run(options)
		} catch (e: RuntimeException) {
			System.err.println(e.message)
			if (options.verbose) {
				System.err.println("Stack trace: ")
				e.printStackTrace()
			}
		}
	}
}