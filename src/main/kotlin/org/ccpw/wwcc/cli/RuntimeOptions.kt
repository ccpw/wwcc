package org.ccpw.wwcc.cli

data class RuntimeOptions(
	val lexerOnly: Boolean,
	val verbose: Boolean,
	val outputFile: String?,
	val inputFile: String,
)