package org.ccpw.wwcc.cli

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.ccpw.wwcc.compiler.lexer.lexeme.PositionedLexeme

object LexemePrinter {
	fun printLexemeList(lexemes: List<PositionedLexeme>): String {
		return Json.encodeToString(lexemes)
	}
}