package org.ccpw.wwcc.util

import kotlinx.serialization.Serializable

@Serializable
data class RayInterval(
	val start: Int,
	val end: Int
) {
	fun inspect(): String {
		return "$start..${end - 1}"
	}
}