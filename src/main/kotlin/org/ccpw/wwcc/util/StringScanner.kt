package org.ccpw.wwcc.util

import org.apache.commons.io.IOUtils
import java.io.InputStream

class StringScanner(stream: InputStream) {
	private var position: Int = 0
	private var string: String = IOUtils.toString(stream, Charsets.UTF_8)

	fun hasMore(): Boolean {
		return (position) < string.length
	}

	fun next(): Char? {
		if (!hasMore()) return null

		return string[position++]
	}

	fun position(): Int {
		return position
	}

	fun unwind(amount: Int) {
		position -= amount
	}
}