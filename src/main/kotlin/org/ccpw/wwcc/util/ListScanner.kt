package org.ccpw.wwcc.util

class ListScanner<E>(
	val list: List<E>
) {
	private var position: Int = 0

	fun next(): E {
		return list[position++]
	}

	fun hasNext(): Boolean {
		return list.size > position
	}
}