package org.ccpw.wwcc.util

object StringUtils {
	fun uniqueStringChars(string: String): String {
		return string.toCharArray().distinct().joinToString("")
	}
}