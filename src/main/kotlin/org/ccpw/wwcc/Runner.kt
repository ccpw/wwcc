package org.ccpw.wwcc

import org.apache.commons.io.FilenameUtils
import org.ccpw.wwcc.cli.LexemePrinter
import org.ccpw.wwcc.cli.RuntimeOptions
import org.ccpw.wwcc.compiler.ast.RootNode
import org.ccpw.wwcc.compiler.codegen.CodeGenerator
import org.ccpw.wwcc.compiler.lexer.RawLexer
import org.ccpw.wwcc.compiler.semantics.CASTPipeline
import org.ccpw.wwcc.compiler.syntaxer.SyntaxParser
import java.io.*
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.system.exitProcess

object Runner {
	const val STDLIB_DIR = "stlib/"

	private fun openOutputStream(outputFile: String): OutputStream {
		if (outputFile == "-") {
			return System.out
		}

		val path = Paths.get(outputFile)

		if (Files.exists(path)) {
			require(Files.isWritable(path)) { "error: '$outputFile' is not writable" }
		} else {
			val baseDir = Paths.get(FilenameUtils.getFullPath(outputFile))

			require(Files.isDirectory(baseDir)) { "error: expected '$baseDir' to be an existing directory" }

			val createdFile = createFile(path)

			requireNotNull(createdFile) { "error: couldn't create '$outputFile', maybe target is not writable" }
			require(Files.isWritable(createdFile)) { "error: '$outputFile' is not writable" }
		}

		return PrintStream(outputFile)
	}

	private fun createFile(path: Path): Path? {
		return try {
			Files.createFile(path)
		} catch (e: IOException) {
			null
		}
	}

	private fun openInputStream(inputFile: String): InputStream {
		if (inputFile == "-") {
			return System.`in`
		}

		require(Files.isReadable(Paths.get(inputFile))) { "error: Couldn't read '$inputFile'" }

		return FileInputStream(inputFile)
	}

	fun loadStdlibDeclarations(): RootNode {
		val options = RuntimeOptions(
			lexerOnly = false,
			verbose = false,
			inputFile = "$STDLIB_DIR/definitions.wwc",
			outputFile = null
		)

		val inputFile = openInputStream(options.inputFile)

		val lexemes = RawLexer(inputFile).process()

		return SyntaxParser.process(lexemes, options) ?: exitProcess(-1)
	}

	fun run(options: RuntimeOptions) {
		val inputFile = openInputStream(options.inputFile)

		val lexemes = RawLexer(inputFile).process()

		if (options.lexerOnly) {
			val outputFile = options.outputFile?.let { openOutputStream(it) } ?: System.out

			val output = LexemePrinter.printLexemeList(lexemes)

			outputFile.write(output.encodeToByteArray())

			outputFile.close()

			return
		}

		val ast = SyntaxParser.process(lexemes, options) ?: exitProcess(-1)
		val stdlibAst = loadStdlibDeclarations()

		val cast = CASTPipeline.process(ast, stdlibAst)

		val files = CodeGenerator.generate(cast)

		files.forEach { (name, contents) ->
			File("compiled/$name.class").writeBytes(contents)
		}
	}
}