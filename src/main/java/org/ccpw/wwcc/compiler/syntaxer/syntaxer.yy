%{
import org.ccpw.wwcc.compiler.lexer.*;
import org.ccpw.wwcc.compiler.lexer.lexeme.*;
import org.ccpw.wwcc.compiler.ast.ghost.*;
import org.ccpw.wwcc.util.*;
import java.util.*;
%}

%token K_CLASS
%token K_EXTENDS
%token K_IS
%token K_END
%token K_VAR
%token K_METHOD
%token K_THIS
%token K_WHILE
%token K_LOOP
%token K_IF
%token K_THEN
%token K_ELSE
%token K_RETURN

%token L_BOOL
%token L_INT
%token L_FLOAT

%token P_LBRACKET
%token P_RBRACKET
%token P_COLON
%token P_ASSIGNMENT
%token P_FSTOP
%token P_COMMA
%token P_LPAREN
%token P_RPAREN

%token IDENTIFIER

%%
Root
    : { $$ = newNode(GhostASTNodeType.ROOT, listOf()); }
    | Root Class { $$ = $1.addChild($2); }
    ;

Class
    : K_CLASS ClassName ClassParent K_IS ClassBody K_END { $$ = newNode(GhostASTNodeType.CLASS, listOf($2, $3, $5)); }
    ;

ClassParent
    : { $$ = newNode(GhostASTNodeType.CLASS_NAME, listOf()); }
    | K_EXTENDS ClassName { $$ = $2; }
    ;

ClassBody
    : { $$ = newNode(GhostASTNodeType.CLASS_BODY, listOf()); }
    | ClassBody ClassStatement { $$ = $1.addChild($2); }
    ;

ClassStatement
    : VarDeclaration
    | CtorDefinition
    | MethodDefinition
    ;

CtorDefinition
    : K_THIS MethodParams K_IS Block K_END { $$ = newNode(GhostASTNodeType.CTOR, listOf($2, $4)); }
    ;

MethodDefinition
    : K_METHOD IDENTIFIER MethodParams MethodReturnType K_IS Block K_END {
        $$ = newNode(GhostASTNodeType.METHOD, listOf($2, $3, $4, $6)) ; }
    ;

MethodReturnType
    : { $$ = newNode(GhostASTNodeType.CLASS_NAME, listOf()); }
    | P_COLON ClassName { $$ = $2; }
    ;

MethodParams
    : { $$ = newNode(GhostASTNodeType.METHOD_PARAMS, listOf()); }
    | P_LPAREN ParamsList P_RPAREN { $$ = $2; }
    ;

ParamsList
    : MethodParam { $$ = newNode(GhostASTNodeType.METHOD_PARAMS, listOf($1)); }
    | ParamsList P_COMMA MethodParam { $$ = $1.addChild($3); }
    ;

MethodParam
    : IDENTIFIER P_COLON ClassName { $$ = newNode(GhostASTNodeType.METHOD_PARAM, listOf($1, $3)); }
    ;

IfClause
    : K_IF Expression K_THEN Block ElseClause K_END { $$ = newNode(GhostASTNodeType.CONDITIONAL, listOf($2, $4, $5)); }
    ;

ElseClause
    : { $$ = emptyBlock(); }
    | K_ELSE Block { $$ = $2; }
    ;

Block
    : { $$ = newNode(GhostASTNodeType.BLOCK, listOf()); }
    | Block Statement { $$ = $1.addChild($2); }
    ;

Statement
    : IfClause
    | WhileClause
    | ReturnStatement
    | VarDeclaration
    | Assignment
    ;

WhileClause
    : K_WHILE Expression K_LOOP Block K_END { $$ = newNode(GhostASTNodeType.LOOP, listOf($2, $4)); }
    ;

ReturnStatement
    : K_RETURN { $$ = newNode(GhostASTNodeType.RETURN, listOf()); }
    | K_RETURN Expression { $$ = newNode(GhostASTNodeType.RETURN, listOf($2)); }
    ;

VarDeclaration
    : K_VAR IDENTIFIER P_COLON Expression { $$ = newNode(GhostASTNodeType.VAR_DECL, listOf($2, $4)); }
    ;

ClassName
    : IDENTIFIER { $$ = newNode(GhostASTNodeType.CLASS_NAME, listOf($1)); }
    | IDENTIFIER P_LBRACKET ClassName P_RBRACKET { $$ = newNode(GhostASTNodeType.CLASS_NAME, listOf($1, $3)); }
    ;

Assignment
    : IDENTIFIER P_ASSIGNMENT Expression { $$ = newNode(GhostASTNodeType.ASSIGNMENT, listOf($1, $3)); }
    ;

Expression
    : Primary
    | CtorCall
    | Expression P_FSTOP MemberCall { $$ = newNode(GhostASTNodeType.INVOCATION, listOf($1, $3)); }
    ;

Primary
    : Literal
    | K_THIS
    | IDENTIFIER
    ;

Literal
    : L_BOOL
    | L_INT
    | L_FLOAT
    ;

MemberCall
    : IDENTIFIER { $$ = newNode(GhostASTNodeType.MEMBER_CALL, listOf($1, newNode(GhostASTNodeType.CALL_ARGS, listOf()))); }
    | IDENTIFIER P_LPAREN CallArgs P_RPAREN { $$ = newNode(GhostASTNodeType.MEMBER_CALL, listOf($1, $3)); }
    ;

CallArgs
    : Expression { $$ = newNode(GhostASTNodeType.CALL_ARGS, listOf($1) ); }
    | CallArgs P_COMMA Expression { $$ = $1.addChild($3); }
    ;

CtorCall
    : ClassName P_LPAREN P_RPAREN { $$ = newNode(GhostASTNodeType.CTOR_CALL, listOf($1, newNode(GhostASTNodeType.CALL_ARGS, listOf()))); }
    | ClassName P_LPAREN CallArgs P_RPAREN { $$ = newNode(GhostASTNodeType.CTOR_CALL, listOf($1, $3)); }
    ;

%%

private ListScanner<PositionedLexeme> lscanner;

void yyerror(String error) {
    System.err.println(error);
    System.err.println(yylval.inspect(1));
}

int yylex() {
    if(!getLscanner().hasNext()) return 0;

    PositionedLexeme lexeme = getLscanner().next();

    switch (lexeme.getType()) {
    // keywords

	case K_CLASS:
	    yylval = newNode(GhostASTNodeType.KEYWORD, lexeme);
	    return K_CLASS;
	case K_EXTENDS:
	    yylval = newNode(GhostASTNodeType.KEYWORD, lexeme);
	    return K_EXTENDS;
	case K_IS:
	    yylval = newNode(GhostASTNodeType.KEYWORD, lexeme);
	    return K_IS;
	case K_END:
	    yylval = newNode(GhostASTNodeType.KEYWORD, lexeme);
	    return K_END;
	case K_VAR:
	    yylval = newNode(GhostASTNodeType.KEYWORD, lexeme);
	    return K_VAR;
	case K_METHOD:
	    yylval = newNode(GhostASTNodeType.KEYWORD, lexeme);
	    return K_METHOD;
	case K_THIS:
	    yylval = newNode(GhostASTNodeType.KEYWORD, lexeme);
	    return K_THIS;
	case K_WHILE:
	    yylval = newNode(GhostASTNodeType.KEYWORD, lexeme);
	    return K_WHILE;
	case K_LOOP:
	    yylval = newNode(GhostASTNodeType.KEYWORD, lexeme);
	    return K_LOOP;
	case K_IF:
	    yylval = newNode(GhostASTNodeType.KEYWORD, lexeme);
	    return K_IF;
	case K_THEN:
	    yylval = newNode(GhostASTNodeType.KEYWORD, lexeme);
	    return K_THEN;
	case K_ELSE:
	    yylval = newNode(GhostASTNodeType.KEYWORD, lexeme);
	    return K_ELSE;
	case K_RETURN:
	    yylval = newNode(GhostASTNodeType.KEYWORD, lexeme);
	    return K_RETURN;

	// punctuation
    case P_LBRACKET:
        yylval = newNode(GhostASTNodeType.PUNCTUATION, lexeme);
        return P_LBRACKET;
    case P_RBRACKET:
        yylval = newNode(GhostASTNodeType.PUNCTUATION, lexeme);
        return P_RBRACKET;
    case P_COLON:
        yylval = newNode(GhostASTNodeType.PUNCTUATION, lexeme);
        return P_COLON;
    case P_ASSIGNMENT:
        yylval = newNode(GhostASTNodeType.PUNCTUATION, lexeme);
        return P_ASSIGNMENT;
    case P_FSTOP:
        yylval = newNode(GhostASTNodeType.PUNCTUATION, lexeme);
        return P_FSTOP;
    case P_COMMA:
        yylval = newNode(GhostASTNodeType.PUNCTUATION, lexeme);
        return P_COMMA;
    case P_LPAREN:
        yylval = newNode(GhostASTNodeType.PUNCTUATION, lexeme);
        return P_LPAREN;
    case P_RPAREN:
        yylval = newNode(GhostASTNodeType.PUNCTUATION, lexeme);
        return P_RPAREN;

    // literals
    case L_BOOL:
        yylval = newNode(GhostASTNodeType.LITERAL, lexeme);
        return L_BOOL;
    case L_INT:
        yylval = newNode(GhostASTNodeType.LITERAL, lexeme);
        return L_INT;
    case L_FLOAT:
        yylval = newNode(GhostASTNodeType.LITERAL, lexeme);
        return L_FLOAT;

    // identifier
    case IDENTIFIER:
        yylval = newNode(GhostASTNodeType.IDENTIFIER, lexeme);
        return IDENTIFIER;
    }
    return -1;
}

public GhostASTNode process() {
    if (getLscanner() == null) throw new RuntimeException("error: no input passed to primary syntaxer");

    int v = yyparse();
    return yyval;
}