package wwc.lang;

public class WWC__Integer {
	private final int value;

	public WWC__Integer Min = new WWC__Integer(java.lang.Integer.MIN_VALUE);
	public WWC__Integer Max = new WWC__Integer(java.lang.Integer.MAX_VALUE);

	WWC__Integer(WWC__Integer a) {
		value = a.value;
	}

	WWC__Integer(WWC__Real a) {
		value = a.toInteger().value;
	}

	WWC__Integer(int a) {
		value = a;
	}

	WWC__Real toReal() {
		return new WWC__Real(value);
	}

	WWC__Integer UnaryMinus() {
		return new WWC__Integer(-value);
	}

	WWC__Integer Plus(WWC__Integer p) {
		return new WWC__Integer(value + p.value);
	}

	WWC__Real Plus(WWC__Real p) {
		return p.Plus(this);
	}

	WWC__Integer Minus(WWC__Integer p) {
		return new WWC__Integer(value - p.value);
	}

	WWC__Real Minus(WWC__Real p) {
		return toReal().Mult(p);
	}

	WWC__Integer Mult(WWC__Integer p) {
		return new WWC__Integer(value * p.value);
	}

	WWC__Real Mult(WWC__Real p) {
		return toReal().Mult(p);
	}

	WWC__Integer Div(WWC__Integer p) {
		return new WWC__Integer(value / p.value);
	}

	WWC__Real Div(WWC__Real p) {
		return toReal().Div(p);
	}

	WWC__Integer Rem(WWC__Integer p) {
		return new WWC__Integer(value % p.value);
	}

	WWC__Boolean Less(WWC__Integer p) {
		return new WWC__Boolean(
				value < p.value
		);
	}

	WWC__Boolean Less(WWC__Real p) {
		return toReal().Less(p);
	}

	WWC__Boolean LessEqual(WWC__Integer p) {
		return new WWC__Boolean(
				value <= p.value
		);
	}

	WWC__Boolean LessEqual(WWC__Real p) {
		return toReal().LessEqual(p);
	}

	WWC__Boolean Greater(WWC__Integer p) {
		return new WWC__Boolean(
				value > p.value
		);
	}

	WWC__Boolean Greater(WWC__Real p) {
		return toReal().Greater(p);
	}

	WWC__Boolean GreaterEqual(WWC__Integer p) {
		return new WWC__Boolean(
				value >= p.value
		);
	}

	WWC__Boolean GreaterEqual(WWC__Real p) {
		return toReal().GreaterEqual(p);
	}

	WWC__Boolean Equal(WWC__Integer p) {
		return new WWC__Boolean(
				value == p.value
		);
	}

	WWC__Boolean Equal(WWC__Real p) {
		return toReal().Equal(p);
	}
}
