package wwc.lang;

public class WWC__Real {
	private final double value;

	public WWC__Real Min = new WWC__Real(java.lang.Double.MIN_VALUE);
	public WWC__Real Max = new WWC__Real(java.lang.Double.MAX_VALUE);
	public WWC__Real Epsilon = new WWC__Real(java.lang.Math.ulp(1.0d));

	WWC__Real(WWC__Real real) {
		value = real.value;
	}

	WWC__Real(WWC__Integer integer) {
		value = integer.toReal().value;
	}

	WWC__Real(double val) {
		value = val;
	}

	WWC__Integer toInteger() {
		return new WWC__Integer((int) value);
	}

	WWC__Real UnaryMinus() {
		return new WWC__Real(-value);
	}

	WWC__Real Plus(WWC__Real p) {
		return new WWC__Real(value + p.value);
	}

	WWC__Real Plus(WWC__Integer p) {
		return Plus(p.toReal());
	}

	WWC__Real Minus(WWC__Real p) {
		return new WWC__Real(value - p.value);
	}

	WWC__Real Minus(WWC__Integer p) {
		return Minus(p.toReal());
	}

	WWC__Real Mult(WWC__Real p) {
		return new WWC__Real(value * p.value);
	}

	WWC__Real Mult(WWC__Integer p) {
		return Mult(p.toReal());
	}

	WWC__Real Div(WWC__Real p) {
		return new WWC__Real(value / p.value);
	}

	WWC__Real Div(WWC__Integer p) {
		return Div(p.toReal());
	}

	WWC__Boolean Less(WWC__Integer p) {
		return new WWC__Boolean(
				value < p.toReal().value
		);
	}

	WWC__Boolean Less(WWC__Real p) {
		return new WWC__Boolean(
				value < p.value
		);
	}

	WWC__Boolean LessEqual(WWC__Integer p) {
		return new WWC__Boolean(
				value <= p.toReal().value
		);
	}

	WWC__Boolean LessEqual(WWC__Real p) {
		return new WWC__Boolean(
				value <= p.value
		);
	}

	WWC__Boolean Greater(WWC__Integer p) {
		return new WWC__Boolean(
				value > p.toReal().value
		);
	}

	WWC__Boolean Greater(WWC__Real p) {
		return new WWC__Boolean(
				value > p.value
		);
	}

	WWC__Boolean GreaterEqual(WWC__Integer p) {
		return new WWC__Boolean(
				value >= p.toReal().value
		);
	}

	WWC__Boolean GreaterEqual(WWC__Real p) {
		return new WWC__Boolean(
				value >= p.value
		);
	}

	WWC__Boolean Equal(WWC__Integer p) {
		return new WWC__Boolean(
				value == p.toReal().value
		);
	}

	WWC__Boolean Equal(WWC__Real p) {
		return new WWC__Boolean(
				value == p.value
		);
	}
}
