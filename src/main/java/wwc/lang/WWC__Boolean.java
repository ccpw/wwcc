package wwc.lang;

public class WWC__Boolean {
	private final boolean value;

	WWC__Boolean(WWC__Boolean b) {
		value = b.value;
	}

	WWC__Boolean(boolean val) {
		value = val;
	}

	WWC__Integer toInteger() {
		return value ? new WWC__Integer(1) : new WWC__Integer(0);
	}

	WWC__Boolean Not() {
		return new WWC__Boolean(!value);
	}

	WWC__Boolean Or(WWC__Boolean b) {
		return new WWC__Boolean(value || b.value);
	}

	WWC__Boolean And(WWC__Boolean b) {
		return new WWC__Boolean(value && b.value);
	}

	WWC__Boolean Xor(WWC__Boolean b) {
		return new WWC__Boolean(value != b.value);
	}
}
