#!/usr/bin/env bash

pushd 'src/main/java/org/ccpw/wwcc/compiler/syntaxer' || exit

byaccj \
  -Jclass=SyntaxAnalyzerGenerated \
  -Jpackage=org.ccpw.wwcc.compiler.syntaxer \
  -Jextends=SyntaxAnalyzerBase \
  -Jsemantic=GhostASTNode \
  -v syntaxer.yy

popd || exit
